#include "src/compiled.h"
#include <stdio.h>
#include <stdlib.h>

typedef uint8_t ind_t;
typedef uint64_t vec_t;

const uint8_t WSIZE = 8;

/* cache for holding orientable rows */
vec_t *B;
size_t bsize;

/* Bott matrix as binary rows */
vec_t *A;

#define C(row,j) ((row>>(n-j-1))&1)
#define M(i,j)   ((A[i]>>(n-j-1))&1)
/*
in src/gaputils.h:24:
#define SWAP(T, a, b) do { T SWAP_TMP = a; a = b; b = SWAP_TMP; } while (0)
*/

/* dimension of Bott manifold */
ind_t n;

/* number of constant rows */
ind_t l;

/* encoding of non-constant rows */
vec_t state;
vec_t max_state;
ind_t init;

void populate_cache(void)
{
    vec_t i, j, o, m;

    bsize = 1<<(n-2-l);
    if (bsize<4) { bsize = 4; }
    B = (vec_t*)malloc(WSIZE*bsize);
    B[0] = 0;
    B[1] = 3;
    B[2] = 5;
    B[3] = 6;
    for (i=4, m=6; i<bsize; ) {
        o = i>>1;
        m <<= 1;
        for (j=i; j<i+o; j++) {
            B[j] = m^B[j-o];
        }
        for (i<<=1, o *= 3; j<i; j++) {
            B[j] = m^B[j-o];
        }
    }
}

Obj BottClear(Obj self)
{
    if (!A) {
        return False;
    }
    free(A);
    A = NULL;
    free(B);
    n = 0;
    return True;
}

Obj BottInit(Obj self, Obj dim, Obj crows)
{
    ind_t c;
    
    BottClear(self);

    n = INT_INTOBJ(dim);
    A = (uint64_t*)malloc(WSIZE*n);
    bzero(A, n*sizeof(uint64_t));
    l = LEN_PLIST(crows);
    for (c=0; c<l; c++) {
        A[c] = INT_INTOBJ(ELM_PLIST(crows, c+1));
    }
    
    state = 0;
    max_state = 1;
    
    for (c=l; c<n-1; c++) {
        max_state <<= (n-c-2);
    }
    max_state -= 1;
    
    populate_cache();

    init = 0;
    return True;
}

Obj BottPrint(Obj self)
{
    ind_t i, j;
    const char c[] = { '.', '1' };

    if (!A) {
        return (Obj)0;
    }
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            printf("%c", c[C(A[i],j)]);
        }
        printf("\n");
    }
    return (Obj)0;
}

Obj BottMat(Obj self)
{
    ind_t i,j;
    Obj mat, row;

    mat = NEW_PLIST(T_PLIST, n);
    SET_LEN_PLIST(mat, n);
    row = NEW_PLIST(T_PLIST, n);
    SET_LEN_PLIST(row, n);
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            SET_ELM_PLIST( row, j+1, INTOBJ_INT(C(A[i],j)));
        }
        SET_ELM_PLIST(mat, i+1, SHALLOW_COPY_OBJ(row) );
        CHANGED_BAG(mat);
    }
    return mat;
}

inline void set()
{
    uint64_t c, j, i, mask;
    for (i=1, c=1, j=n-3; ; c+=i++, j--) {
        mask = (1<<i) - 1;
        A[j] = B[(state>>(c-1))&mask];
        //printf("%u-%lu ", mask, (state>>(c-1))&mask);
        if (j==l) {
            break;
        }
    }
}

Obj BottNext(Obj self)
{
    if (state == max_state || !A) {
        return Fail;
    }
    if (init) {
        state++;
    } else {
        init = 1;
    }
    set();
    return True;
}

Obj BottSetState(Obj self, Obj s)
{
    vec_t candidate = INT_INTOBJ(s);
    if (!A || candidate<0 || candidate>=max_state) {
        return Fail;
    }
    init = 1;
    state = candidate;
    set();
    return s;
}

Obj BottGetState(Obj self)
{
    if (!A) {
        return Fail;
    }
    return INTOBJ_INT(state);
}

inline int equal_cols(uint64_t i, uint64_t j)
{
    uint64_t si, sj, r;

    if (i==j) {
        return 1;
    }
    if (i>j) {
        SWAP(uint64_t,i,j);
    }
    si = n-i-1;
    sj = n-j-1;

    for (r=0; r<j; r++) {
        if ( ((A[r]>>si)&1) != ((A[r]>>sj)&1) ) {
            return 0;
        }
    }
    return 1;
}

inline vec_t scalar_product(ind_t i, ind_t j)
{
    vec_t r = A[i]&A[j], s, k;
    if (i>j) {
        SWAP(uint64_t,i,j);
    }
    for (k=j+1, s=0; k<n; k++) {
        s ^= C(r,k);
    }
    return s;
}

Obj BottIsSpinc(Obj self)
{
    ind_t i, j, e, z;
    vec_t aij;
    for (j=2; j<n-2; j++) {
        e = 1;
        z = 1;
        for (i=0; i<j; i++) {
            if (equal_cols(j,i)) {
                aij = 0;
            } else {
                aij = scalar_product(i, j);
            }
            if (z && aij) {
                z = 0;
            }
            if (e && aij!=C(A[i],j)) {
                e = 0;
            }
            if (!e && !z) {
                return False;
            }
        }
    }
    return True;
}

inline vec_t row_sum(ind_t i)
{
    ind_t j;
    vec_t s;
    for (s=0, j=i+1; j<n; j++) {
        s += C(A[i],j);
    }
    return s;
}

Obj BottIsSpin(Obj self)
{
    ind_t i,j;
    for (j=1; j<n-2; j++) {
        if (row_sum(j)%4 == 2) {
            for (i=0; i<j; i++) {
                if (scalar_product(i, j) != M(i,j)) {
                    return False;
                }
            }
        } else {
            for (i=0; i<j; i++) {
                if (scalar_product(i, j)==1) {
                    return False;
                }
            }
        }
    }
    return True;
}

/* 
 * GVarFunc - list of functions to export
 */
static StructGVarFunc GVarFunc[] = {
    { "BottInit" ,     2, "dim, crows", BottInit     , "bott.c:BottInit"     },
    { "BottClear",     0, ""          , BottClear    , "bott.c:BottClear"    },
    { "BottPrint",     0, ""          , BottPrint    , "bott.c:BottPrint"    },
    { "BottMat"  ,     0, ""          , BottMat      , "bott.c:BottMat"      },
    { "BottNext" ,     0, ""          , BottNext     , "bott.c:BottNext"     },
    { "BottSetState" , 1, "state"     , BottSetState , "bott.c:BottSetState" },
    { "BottGetState" , 0, ""          , BottGetState , "bott.c:BottGetState" },
    { "BottIsSpinc"  , 0, ""          , BottIsSpinc  , "bott.c:BottIsSpinc"  },
    { "BottIsSpin"   , 0, ""          , BottIsSpin   , "bott.c:BottIsSpin"   },
    { 0 }
};

static Int InitKernel (StructInitInfo * module)
{
    InitHdlrFuncsFromTable(GVarFunc);
    return 0;
}

static Int InitLibrary(StructInitInfo * module)
{
    InitGVarFuncsFromTable(GVarFunc);

    A = NULL;
    n = 0;

    return 0;
}

static StructInitInfo module = {
    MODULE_DYNAMIC,
    "shared",
    0,
    0,
    0,
    0,
    InitKernel,
    InitLibrary,
    0,
    0,
    0,
    0
};

StructInitInfo * Init__Dynamic(void)
{
  return &module;
}
