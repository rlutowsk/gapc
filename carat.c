#include "src/compiled.h"

#include "typedef.h"
#include "getput.h"
#include "matrix.h"
#include "bravais.h"
#include "datei.h"
#include "voronoi.h"

void free_mats( matrix_TYP **l, int no )
{
	for (int i=0; i<no; i++) {
		free_mat( l[i] );
	}
}

matrix_TYP *matrix_plist( Obj mat, int *dim )
{
	matrix_TYP *m;
	Obj       row;
	int       i,j;

	*dim = LEN_PLIST( mat );

	if ( *dim == 0 ) {
		return NULL;
	}

	m = init_mat( *dim, *dim, "" );

	for ( i=0; i<*dim; i++ ) {
		row = ELM_PLIST(mat, i+1);
		for ( j=0; j<*dim; j++ ) {
			m->array.SZ[i][j] = INT_INTOBJ( ELM_PLIST( row, j+1 ) );
		}
	}
	return m;
}

matrix_TYP **carat_plist( Obj list , int *len)
{
	matrix_TYP **mats;
	Obj mat, row;
	Int i,j,k,n;

	*len = LEN_PLIST( list );
	if ( *len == 0 ) {
		return NULL;
	}
	
	mat = ELM_PLIST( list, 1 );
	n   = LEN_PLIST( mat );
	if ( n == 0 ) {
		*len = 0;
		return NULL;
	}

	mats = (matrix_TYP **)malloc( *len * sizeof(matrix_TYP*) );
	if ( mats == NULL ) {
		return NULL;
	}

	for (k=0; k<*len; k++) {
		mat     = ELM_PLIST( list, k+1 );
		mats[k] = init_mat( n, n, "" );
		for (i=0; i<n; i++) {
			row = ELM_PLIST( mat, i+1 );
			for (j=0;j<n;j++) {
				mats[k]->array.SZ[i][j] = INT_INTOBJ( ELM_PLIST( row, j+1) );
			}
		}
	}
	return mats;
}


#ifndef _GETPUT_BRAVAIS_FIELD
#define _GETPUT_BRAVAIS_FIELD

#define GET_BRAVAIS_FIELD( G, C ) \
	if ( ISB_REC( rec, RNamName(G) ) ) { \
		br->C = carat_plist( ELM_REC(rec, RNamName(G) ), &br->C##_no ); \
	} else { \
		br->C = NULL; \
		br->C##_no = 0; \
	}

#define PUT_BRAVAIS_FIELD( G, C, N ) \
	SET_RNAM_PREC( *rec, N, RNamName( G  ) ); \
  CHANGED_BAG( *rec ); \
	plist_carat ( &tmp, br->C, br->C##_no, br->dim ); \
	SET_ELM_PREC( *rec, N, SHALLOW_COPY_OBJ( tmp ) ); \
  CHANGED_BAG( *rec );

#endif // _GETPUT_BRAVAIS_FIELD

bravais_TYP *carat_record( Obj rec )
{
	Obj tmp;
	bravais_TYP *br;// = malloc( sizeof(bravais_TYP) );

	if ( ! IS_REC( rec ) ) {
		return NULL;
	}

	if ( ! ISB_REC( rec, RNamName("generators") ) ) {
		return NULL;
	}
	tmp = ELM_REC( rec, RNamName("generators") );
	if ( ! IS_DENSE_PLIST( tmp ) ) {
		return NULL;
	}	
	br = (bravais_TYP*) malloc( sizeof( bravais_TYP ) );
	if ( !br ) {
		return NULL;
	}
	br->dim = LEN_PLIST( ELM_PLIST( tmp, 1 ) );

	br->gen = carat_plist( tmp, &br->gen_no );
	if ( br->gen_no == 0 ) {
		free_bravais( br );
		return NULL;
	}

	if ( ISB_REC( rec, RNamName("size") ) ) {
		br->order = INT_INTOBJ( ELM_REC( rec, RNamName("size") ) );
	}	

	/*
	if ( ISB_REC( rec, RNamName("formspace") ) ) {
		br->form = carat_plist( ELM_REC( rec, RNamName("formspace") ), &br->form_no );
	} else {
		br->form_no = 0;
		br->form    = NULL;
	}
	*/
	GET_BRAVAIS_FIELD( "formspace"  , form   );
	GET_BRAVAIS_FIELD( "centerings" , zentr  );
	GET_BRAVAIS_FIELD( "normalizer" , normal );
	GET_BRAVAIS_FIELD( "centralizer", cen    );

	return br;
}

Obj plist_matrix( Obj *out, matrix_TYP *mat )
{
	Obj row;
	int i,j;

	*out = NEW_PLIST( T_PLIST, mat->rows );
	row  = NEW_PLIST( T_PLIST, mat->cols );
	SET_LEN_PLIST( *out, (Int)mat->rows );
	CHANGED_BAG( *out );
	SET_LEN_PLIST( row , (Int)mat->cols );
	CHANGED_BAG( row );
	for (i=0; i<mat->rows;i++) {
		for (j=0;j<mat->cols;j++) {
			SET_ELM_PLIST( row, j+1, INTOBJ_INT( mat->array.SZ[i][j] ) );
			CHANGED_BAG( row );
		}
		SET_ELM_PLIST( *out, i+1, SHALLOW_COPY_OBJ( row ) );
		CHANGED_BAG( *out );
	}
	return *out;
}

void plist_carat( Obj *L, matrix_TYP **mats, Int len, Int dim )
{
	Obj mat, row;
	Int i,j,k;

	*L = NEW_PLIST( T_PLIST, len );
	SET_LEN_PLIST( *L, len );
	CHANGED_BAG( *L );
	
	mat = NEW_PLIST( T_PLIST, dim );
	row = NEW_PLIST( T_PLIST, dim );
	SET_LEN_PLIST( mat, dim );
	CHANGED_BAG( mat );
	SET_LEN_PLIST( row, dim );
	CHANGED_BAG( row );

	for (k=0; k<len; k++) {
		for (i=0; i<dim; i++) {
			for (j=0; j<dim; j++) {
				SET_ELM_PLIST( row, j+1, INTOBJ_INT( mats[k]->array.SZ[i][j] ) );
				CHANGED_BAG( row );
			}
			SET_ELM_PLIST( mat, i+1, SHALLOW_COPY_OBJ( row ) );
			CHANGED_BAG( mat );
		}
		SET_ELM_PLIST( *L, k+1, SHALLOW_COPY_OBJ( mat ) );
		CHANGED_BAG( *L );
	}
}

Obj record_carat( Obj *rec, bravais_TYP *br )
{
	Obj tmp;

	*rec = NEW_PREC( 6 );

	SET_RNAM_PREC( *rec, 1, RNamName( "size"        ) );
	CHANGED_BAG( *rec );
	tmp = INTOBJ_INT( br->order );
	SET_ELM_PREC( *rec, 1, SHALLOW_COPY_OBJ( tmp ) );
	CHANGED_BAG( *rec );

	PUT_BRAVAIS_FIELD( "generators" , gen   , 2 );
	PUT_BRAVAIS_FIELD( "formspace"  , form  , 3 );
	PUT_BRAVAIS_FIELD( "centerings" , zentr , 4 );
	PUT_BRAVAIS_FIELD( "normalizer" , normal, 5 );
	PUT_BRAVAIS_FIELD( "centralizer", cen   , 6 );

	SET_LEN_PREC( *rec, 6 );
	CHANGED_BAG( *rec );

	return *rec;
}

Obj FuncNORMALIZER(Obj self, Obj gens)
{
	bravais_TYP *grp;
	Obj mat, N;
	Int i,n;

	grp = (bravais_TYP *) malloc(sizeof(bravais_TYP));
	if (grp == NULL) {
		return Fail;
	}

	grp->gen = carat_plist( gens, &grp->gen_no );
	if ( grp->gen == NULL ) {
		free_bravais( grp );
		return Fail;
	}
	
	mat = ELM_PLIST( gens, 1 );
	n   = LEN_PLIST( mat ); // assume all matrices are n x n
	if ( n == 0 ) {
		free_bravais( grp );
		return Fail;
	}
	
	for (i=0; i<100; i++) grp->divisors[i] = 0;
	grp->dim = n;
	
	grp->form_no  = 0;
	grp->form     = NULL;
	grp->zentr_no = 0;
	grp->zentr    = NULL;
	grp->normal_no= 0;
	grp->normal   = NULL;
	grp->cen_no   = 0;
	grp->cen      = NULL;

	normalisator( grp, NULL, NULL, 1949, 0, 0 );

	record_carat( &N, grp );

	free_bravais( grp );
	return N;
}

Obj gap_bravais_group( Obj self, Obj rec, Obj flag )
{
	Int           tf = INT_INTOBJ( flag ) ? TRUE : FALSE;
	bravais_TYP *grp = carat_record( rec );
	bravais_TYP *br;
	Obj          ret;

	br = bravais_group( grp, tf );

	record_carat( &ret, br );
	free_bravais( grp );
	free_bravais( br  );
	return ret;
}

Obj gap_formspace( Obj self, Obj gens, Obj flag )
{
	int        gno, fno, n;
	Int         fl = INT_INTOBJ( flag );
	matrix_TYP **g = carat_plist( gens , &gno);
	matrix_TYP **f;
	Obj         fs;

	f = formspace( g, gno, fl, &fno );
	n = LEN_PLIST( ELM_PLIST( gens, 1 ) ); 
	plist_carat( &fs, f, fno, n );

	free_mats( g, gno );
	free_mats( f, fno );

	return fs;
}

Obj gap_first_perfect(Obj self, Obj gA, Obj gG, Obj gFtr, Obj gtrbifo )
{
	matrix_TYP *fp;
	int        min, dim, tmp;
	matrix_TYP *A, **Ftr, *trbifo;
	bravais_TYP *G;

	Obj rec, gfp;

	A   = matrix_plist( gA, &dim );
	G   = carat_record( gG );
	Ftr = carat_plist( gFtr, &tmp );
	trbifo = matrix_plist( gtrbifo, &dim );
	fp = first_perfect( A, G, Ftr, trbifo, &min );

	plist_matrix( &gfp, fp );

	rec = NEW_PREC( 2 );
	SET_RNAM_PREC( rec, 1, RNamName( "form" ) );
	CHANGED_BAG( rec );
	SET_ELM_PREC(  rec, 1, gfp );
	CHANGED_BAG( rec );
	SET_RNAM_PREC( rec, 2, RNamName( "min" ) );
	CHANGED_BAG( rec );
	SET_ELM_PREC(  rec, 2, INTOBJ_INT( min ) );
	CHANGED_BAG( rec );

	return rec;
}

Obj gap_normalizer( Obj self, Obj gA, Obj gGB, Obj gGtr, Obj gprime, Obj gVanz )
{

}

/* 
 * GVarFunc - list of functions to export
 */
static StructGVarFunc GVarFunc[] = {
  
    { "C_NORMALIZER_IN_GL_N_Z", 1, "gens"             , FuncNORMALIZER    , "carat.c:NormalizerInGLnZ" },
	{ "C_BRAVAIS_GROUP"       , 2, "rec, flag"        , gap_bravais_group , "carat.c:BravaisGroup"     }, 
	{ "C_FORMSPACE"           , 2, "gens, flag"       , gap_formspace     , "carat.c:Formspace"        },
	{ "C_FIRST_PERFECT"       , 4, "A, G, Ftr, trbifo", gap_first_perfect , "carat.c:FirstPerfect"     },
	{ 0 }
};

static Int InitKernel (StructInitInfo * module)
{
    InitHdlrFuncsFromTable(GVarFunc);
    return 0;
}

static Int InitLibrary(StructInitInfo * module)
{
    InitGVarFuncsFromTable(GVarFunc);
    return 0;
}

static StructInitInfo module = {
    MODULE_DYNAMIC,
    "fimm",
    0,
    0,
    0,
    0,
    InitKernel,
    InitLibrary,
    0,
    0,
    0,
    0
};

StructInitInfo * Init__Dynamic(void)
{
    return &module;
}

