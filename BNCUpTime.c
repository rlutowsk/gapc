#include "src/compiled.h"

#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

Obj BNCUpTime()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return INTOBJ_INT(tv.tv_sec*1000 + tv.tv_usec/1000);
}

/* 
 * GVarFunc - list of functions to export
 */
static StructGVarFunc GVarFunc[] = {
  
  { "BNCUpTime", 0, "", BNCUpTime, "BNCUpTime.c:BNCUpTime" },
  { 0 }
};

static Int InitKernel (StructInitInfo * module)
{
  InitHdlrFuncsFromTable(GVarFunc);
  return 0;
}

static Int InitLibrary(StructInitInfo * module)
{
  InitGVarFuncsFromTable(GVarFunc);
  return 0;
}

static StructInitInfo module = {
  MODULE_DYNAMIC,
  "BNCUpTime",
  0,
  0,
  0,
  0,
  InitKernel,
  InitLibrary,
  0,
  0,
  0,
  0
};

StructInitInfo * Init__Dynamic(void)
{
  return &module;
}

StructInitInfo * Init__BNCUpTime(void)
{
  return &module;
}

#ifdef __cplusplus
}
#endif
