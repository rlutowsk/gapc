// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Benoit Jacob <jacob.benoit.1@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

// This C++ file compiles to binary code that can be linked to by your C program,
// thanks to the extern "C" syntax used in the declarations in binary_library.h.

#include "eigen.h"

#include <Eigen/Core>

using namespace Eigen;

typedef Matrix<entry_t, Dynamic, Dynamic, RowMajor> MatrixXc;

/************************* pointer conversion methods **********************************************/

////// class MatrixXc //////

inline MatrixXc& c_to_eigen(C_MatrixXc* ptr)
{
  return *reinterpret_cast<MatrixXc*>(ptr);
}

inline const MatrixXc& c_to_eigen(const C_MatrixXc* ptr)
{
  return *reinterpret_cast<const MatrixXc*>(ptr);
}

inline C_MatrixXc* eigen_to_c(MatrixXc& ref)
{
  return reinterpret_cast<C_MatrixXc*>(&ref);
}

inline const C_MatrixXc* eigen_to_c(const MatrixXc& ref)
{
  return reinterpret_cast<const C_MatrixXc*>(&ref);
}

////// class Map<MatrixXc> //////

inline Map<MatrixXc>& c_to_eigen(C_Map_MatrixXc* ptr)
{
  return *reinterpret_cast<Map<MatrixXc>*>(ptr);
}

inline const Map<MatrixXc>& c_to_eigen(const C_Map_MatrixXc* ptr)
{
  return *reinterpret_cast<const Map<MatrixXc>*>(ptr);
}

inline C_Map_MatrixXc* eigen_to_c(Map<MatrixXc>& ref)
{
  return reinterpret_cast<C_Map_MatrixXc*>(&ref);
}

inline const C_Map_MatrixXc* eigen_to_c(const Map<MatrixXc>& ref)
{
  return reinterpret_cast<const C_Map_MatrixXc*>(&ref);
}


/************************* implementation of classes **********************************************/


////// class MatrixXc //////


C_MatrixXc* MatrixXc_new(int rows, int cols)
{
  return eigen_to_c(*new MatrixXc(rows,cols));
}

void MatrixXc_delete(C_MatrixXc *m)
{
  delete &c_to_eigen(m);
}

entry_t* MatrixXc_data(C_MatrixXc *m)
{
  return c_to_eigen(m).data();
}

void MatrixXc_set_zero(C_MatrixXc *m)
{
  c_to_eigen(m).setZero();
}

void MatrixXc_resize(C_MatrixXc *m, int rows, int cols)
{
  c_to_eigen(m).resize(rows,cols);
}

void MatrixXc_copy(C_MatrixXc *dst, const C_MatrixXc *src)
{
  c_to_eigen(dst) = c_to_eigen(src);
}

void MatrixXc_copy_map(C_MatrixXc *dst, const C_Map_MatrixXc *src)
{
  c_to_eigen(dst) = c_to_eigen(src);
}

void MatrixXc_set_coeff(C_MatrixXc *m, int i, int j, entry_t coeff)
{
  c_to_eigen(m)(i,j) = coeff;
}

entry_t MatrixXc_get_coeff(const C_MatrixXc *m, int i, int j)
{
  return c_to_eigen(m)(i,j);
}

void MatrixXc_print(const C_MatrixXc *m)
{
  std::cout << c_to_eigen(m) << std::endl;
}

void MatrixXc_multiply(const C_MatrixXc *m1, const C_MatrixXc *m2, C_MatrixXc *result)
{
  c_to_eigen(result) = c_to_eigen(m1) * c_to_eigen(m2);
}

void MatrixXc_add(const C_MatrixXc *m1, const C_MatrixXc *m2, C_MatrixXc *result)
{
  c_to_eigen(result) = c_to_eigen(m1) + c_to_eigen(m2);
}



////// class Map_MatrixXc //////


C_Map_MatrixXc* Map_MatrixXc_new(entry_t *array, int rows, int cols)
{
  return eigen_to_c(*new Map<MatrixXc>(array,rows,cols));
}

void Map_MatrixXc_delete(C_Map_MatrixXc *m)
{
  delete &c_to_eigen(m);
}

void Map_MatrixXc_set_zero(C_Map_MatrixXc *m)
{
  c_to_eigen(m).setZero();
}

void Map_MatrixXc_copy(C_Map_MatrixXc *dst, const C_Map_MatrixXc *src)
{
  c_to_eigen(dst) = c_to_eigen(src);
}

void Map_MatrixXc_copy_matrix(C_Map_MatrixXc *dst, const C_MatrixXc *src)
{
  c_to_eigen(dst) = c_to_eigen(src);
}

void Map_MatrixXc_set_coeff(C_Map_MatrixXc *m, int i, int j, entry_t coeff)
{
  c_to_eigen(m)(i,j) = coeff;
}

entry_t Map_MatrixXc_get_coeff(const C_Map_MatrixXc *m, int i, int j)
{
  return c_to_eigen(m)(i,j);
}

void Map_MatrixXc_print(const C_Map_MatrixXc *m)
{
  std::cout << c_to_eigen(m) << std::endl;
}

void Map_MatrixXc_multiply(const C_Map_MatrixXc *m1, const C_Map_MatrixXc *m2, C_Map_MatrixXc *result)
{
  c_to_eigen(result) = c_to_eigen(m1) * c_to_eigen(m2);
}

void Map_MatrixXc_add(const C_Map_MatrixXc *m1, const C_Map_MatrixXc *m2, C_Map_MatrixXc *result)
{
  c_to_eigen(result) = c_to_eigen(m1) + c_to_eigen(m2);
}

int Map_MatrixXc_cmp(const C_Map_MatrixXc *m1, const C_Map_MatrixXc *m2)
{
  return c_to_eigen(m1) == c_to_eigen(m2);
}