// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Benoit Jacob <jacob.benoit.1@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

// This is a pure C header, no C++ here.
// The functions declared here will be implemented in C++ but
// we don't have to know, because thanks to the extern "C" syntax,
// they will be compiled to C object code.

#ifndef SMITH_EIGEN_H
#define SMITH_EIGEN_H

#ifdef __cplusplus
#include <iostream>
#include <common.h>
extern "C"
{
#endif

  // just dummy empty structs to give different pointer types,
  // instead of using void* which would be type unsafe
  struct C_MatrixXc {};
  struct C_Map_MatrixXc {};

  // the C_MatrixXc class, wraps some of the functionality
  // of Eigen::MatrixXc.
  struct C_MatrixXc* MatrixXc_new(int rows, int cols);
  void    MatrixXc_delete     (struct C_MatrixXc *m);
  entry_t* MatrixXc_data       (struct C_MatrixXc *m);
  void    MatrixXc_set_zero   (struct C_MatrixXc *m);
  void    MatrixXc_resize     (struct C_MatrixXc *m, int rows, int cols);
  void    MatrixXc_copy       (struct C_MatrixXc *dst,
                               const struct C_MatrixXc *src);
  void    MatrixXc_copy_map   (struct C_MatrixXc *dst,
                               const struct C_Map_MatrixXc *src);  
  void    MatrixXc_set_coeff  (struct C_MatrixXc *m,
                               int i, int j, entry_t coeff);
  entry_t  MatrixXc_get_coeff  (const struct C_MatrixXc *m,
                               int i, int j);
  void    MatrixXc_print      (const struct C_MatrixXc *m);
  void    MatrixXc_add        (const struct C_MatrixXc *m1,
                               const struct C_MatrixXc *m2,
                               struct C_MatrixXc *result);  
  void    MatrixXc_multiply   (const struct C_MatrixXc *m1,
                               const struct C_MatrixXc *m2,
                               struct C_MatrixXc *result);
  
  // the C_Map_MatrixXc class, wraps some of the functionality
  // of Eigen::Map<MatrixXc>
  struct C_Map_MatrixXc* Map_MatrixXc_new(entry_t *array, int rows, int cols);
  void   Map_MatrixXc_delete     (struct C_Map_MatrixXc *m);
  void   Map_MatrixXc_set_zero   (struct C_Map_MatrixXc *m);
  void   Map_MatrixXc_copy       (struct C_Map_MatrixXc *dst,
                                  const struct C_Map_MatrixXc *src);
  void   Map_MatrixXc_copy_matrix(struct C_Map_MatrixXc *dst,
                                  const struct C_MatrixXc *src);  
  void   Map_MatrixXc_set_coeff  (struct C_Map_MatrixXc *m,
                                  int i, int j, entry_t coeff);
  entry_t Map_MatrixXc_get_coeff  (const struct C_Map_MatrixXc *m,
                                  int i, int j);
  void   Map_MatrixXc_print      (const struct C_Map_MatrixXc *m);
  void   Map_MatrixXc_add        (const struct C_Map_MatrixXc *m1,
                                  const struct C_Map_MatrixXc *m2,
                                  struct C_Map_MatrixXc *result);  
  void   Map_MatrixXc_multiply   (const struct C_Map_MatrixXc *m1,
                                  const struct C_Map_MatrixXc *m2,
                                  struct C_Map_MatrixXc *result);

int Map_MatrixXc_cmp(const struct C_Map_MatrixXc *m1, const struct C_Map_MatrixXc *m2);

#ifdef __cplusplus
} // end extern "C"
#endif

#endif // SMITH_EIGEN_H
