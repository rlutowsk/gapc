#include <stdio.h>
#include <stdlib.h>

#ifndef ASNF_COLTRANS
#define ASNF_COLTRANS
#endif

#include "common.h"
#include "utils.h"
#include "matrix.h"
#include "asnf.h"
#include "mm.h"
#include "eigen.h"

int main(int argc, char *argv[])
{
	char *filename = NULL;
	char *dimension = NULL;
	matrix_t *mat = NULL;
	matrix_t *l = NULL, *r = NULL;
	matrix_t *m1 = NULL, *m2 = NULL;
	struct C_Map_MatrixXc *el, *emat, *er, *em1, *em2;
	long ntime = 0, mtime = 0;
	int nloops=1, opt, show_time = 0, verbose = 0;
	int m=1600,n=6400;
	int b = 0, use_pb = 0;
	long lastsec = 0;
	struct timeval lasttime;

	use_log = 0;
    omp_set_num_threads(NUM_THREADS);

	while ((opt = getopt(argc, argv, "phblvtn:f:d:j:")) != -1) {
		switch (opt) {
		case 'b':
			b = 1;
			break;
        case 'j':
            omp_set_num_threads(atoi(optarg));
		case 'd':
			dimension = optarg;
			if (sscanf(dimension, "%dx%d", &m,&n)==EOF) {
				fprintf(stderr, "dimension must be in the form mxn");
				exit(1);
			}
			break;
		case 'p': 
			use_pb = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		case 't':
			show_time = 1;
			break;
		case 'l':
			use_log = 1;
			break;
		case 'n':
			nloops = atoi(optarg);
			if (nloops<0) {
				nloops = 1;
			}
			break;
		case 'f':
			filename = optarg;
			break;
		case 'h':
		default: /* '?' */
			fprintf(stderr, 
				"Usage: %s [{-f filename | -d rowsxcols}] [-j njobs] [-n loops] [-v] [-l] [-t] [-b] [-p]\n\n"
				"\t-v show all\n"
				"\t-l show progress\n"
				"\t-t show time\n"
				"\t-b break result\n"
				"\t-p show progress", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	if (verbose) {
		use_log = 1;
		show_time = 1;
	}

	if (use_pb) {
		use_log = 0;
	}
	for (dim_t i=0; i<nloops; i++) { 
		flog("\33[2K\r");
		flog("loop %lu: g", i);

        if (filename) {
		    mat = read_from_file_matrix(filename);
			flog("f ");
		}
		else {
			flog("r ");
			mat	= random_matrix(m,n);
		}
		flog("lr ");
		l = random_matrix(mat->nrows, mat->nrows);
        r = random_matrix(mat->ncols, mat->ncols);
        flog("mm ");
		tic();
		m1 = multiply_matrices(l, mat);
		m2 = multiply_matrices(m1, r);
		ntime += toc();

        flog("eg ");
		el   = Map_MatrixXc_new(l->list, l->nrows, l->ncols);
		emat = Map_MatrixXc_new(mat->list, mat->nrows, mat->ncols);
		er   = Map_MatrixXc_new(r->list, r->nrows, r->ncols);
		em1  = (struct C_Map_MatrixXc*)MatrixXc_new(mat->nrows, mat->ncols);
		em2  = (struct C_Map_MatrixXc*)MatrixXc_new(mat->nrows, mat->ncols);
		flog("emm ");
		tic();
		Map_MatrixXc_multiply(el, emat, em1);
		Map_MatrixXc_multiply(em1, er, em2);
		mtime += toc();
		if (b) {
			m2->rows[0][0]+=1;	
		}
		flog("edn ");
		Map_MatrixXc_delete(em1);
		em1 = Map_MatrixXc_new(m2->list, m2->nrows, m2->ncols);

		flog("ecmp ");
        if (!Map_MatrixXc_cmp(em2, em1)) {
            display_matrix_gap("left", l);
            display_matrix_gap("mat", mat);
			display_matrix_gap("right", r);
            i = nloops;
        }

		flog("f ");
		free_matrix(mat);
		mat = NULL;
		free_matrix(l);
		l = NULL;
		free_matrix(r);
		r = NULL;
		free_matrix(m1);
		m1 = NULL;
		free_matrix(m2);
		m2 = NULL;
		Map_MatrixXc_delete(emat);
		Map_MatrixXc_delete(el);
		Map_MatrixXc_delete(er);
		Map_MatrixXc_delete(em1);
		Map_MatrixXc_delete(em2);
		if (use_pb) {
			gettimeofday(&lasttime,NULL);
			if (lasttime.tv_sec > lastsec) {
				lastsec = lasttime.tv_sec;
				fprintf(stderr, "%10ld/%d\r", i, nloops);
			}
		}
    }
    flog("\n");
	if (show_time) {
		fprintf(stderr,"Time spent on mm   : %4ums (%ldns)\n", (unsigned int)(ntime/1000000), ntime);
		fprintf(stderr,"Time spent on eigen: %4ums (%ldns)\n", (unsigned int)(mtime/1000000), mtime);
	}
	
	return 0;
}
