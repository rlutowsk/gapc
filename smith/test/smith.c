#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "utils.h"
#include "matrix.h"
#include "asnf.h"
#include "mm.h"

int main(int argc, char *argv[])
{
	char *filename = NULL;
	char *dimension = NULL;
	matrix_t *mat = NULL;
	matrix_t *copy1 = NULL;
	matrix_t	 *ns1 = NULL;
	matrix_t *mult1 = NULL;
	long ntime = 0, mtime = 0;
	pid_t pid = getpid();
	int nloops=1, opt, show_time = 0, multiply = 0, multiply_check = 0, verbose = 0, test = 0;
	int m=1600,n=6400;

	use_log = 0;
#ifdef _USE_OMP
    omp_set_num_threads(NUM_THREADS);
#endif

	while ((opt = getopt(argc, argv, "vcmtln:f:pd:j:")) != -1) {
		switch (opt) {
        case 'j':
#ifdef _USE_OMP
    omp_set_num_threads(atoi(optarg));
#endif
		case 'd':
			dimension = optarg;
			if (sscanf(dimension, "%dx%d", &m,&n)==EOF) {
				fprintf(stderr, "dimension must be in the form mxn");
				exit(1);
			}
			break;
		case 'p':
			test = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'm':
			multiply = 1;
			break;
		case 'c':
			multiply_check = 0;
			break;
		case 't':
			show_time = 1;
			break;
		case 'l':
			use_log = 1;
			break;
		case 'n':
			nloops = atoi(optarg);
			if (nloops<0) {
				nloops = 1;
			}
			break;
		case 'f':
			filename = optarg;
			break;
		default: /* '?' */
			fprintf(stderr, "Usage: %s [-c] [-m] [-t] [-l] [-n loops] [-f name]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	if (verbose) {
		use_log = 1;
		show_time = 1;
	}

	if (test) {
		matrix_t *m1 = NULL; //identity_matrix(3);
		entry_t l[] = {1,2,3,4,5,6,7,8,9,10,11,12};
		matrix_t *m2 = from_list_matrix(l,3,4);

		display_matrix(m2);
		swap_cols(m2, 2, 3);
		swap_rows(m2, 0, 1);
		display_matrix(m2);

		if (dimension) {
			free_matrix(m1);
			free_matrix(m2);
			//m1 = identity_matrix(m);
			m2 = random_matrix(m,n);
		}
		printf("m:="); display_matrix_gap(NULL, m2);
		m1 = nullspace_mat_mod_n(m2,128);
		printf("n:="); display_matrix_gap(NULL, m2);
		printf("r:="); display_matrix_gap(NULL, m1);

		free_matrix(m1);
		free_matrix(m2);
		return 0;
	}
	for (dim_t i=0; i<nloops; i++) {
		flog("loop %lu: g", i);

		mat = read_from_file_matrix(filename);
		flog("f ");
		if (!mat) {
			flog("\b\br ");
			mat	= random_matrix(m,n);
		}
		flog("c1 ");
		copy1 = structural_copy_mat(mat);
		flog("n1 ");
		tic();
		ns1 = nullspace_mat_mod_n(copy1, 128);
		ntime += toc();
		if (multiply) {
			flog("m ");
			tic();
			mult1 = multiply_matrices(ns1, mat);
			mtime += toc();
			if (multiply_check) {
				flog("z ");
				if ( !is_zero_mat_mod_n(mult1, 128)) {
					printf(",\nns1 := ");
					display_matrix_gap(NULL, ns1);
					printf(",\ncopy1 := ");
					display_matrix_gap(NULL, copy1);
					printf(",\nmat := ");
					display_matrix_gap(NULL, mat);
					printf(");\n");
					i = nloops; // stop working
				}
			}
			free_matrix(mult1);
		}
		flog("f ");
		free_matrix(mat);
		free_matrix(ns1);
		free_matrix(copy1);
		flog("\n");
	}
	if (show_time) {
		fprintf(stderr,"[%u] Time spent on nulls_new calc: %4ums (%ldns)\n", pid, (unsigned int)(ntime/1000000), ntime);
		if (multiply) {
			fprintf(stderr,"[%u] Time spent on multiplication: %4ums (%ldns)\n", pid, (unsigned int)(mtime/1000000), mtime);
		}
	}
	return 0;
}
