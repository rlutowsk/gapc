#include <stdio.h>
#include <stdlib.h>

#ifndef ASNF_COLTRANS
#define ASNF_COLTRANS
#endif

#include "common.h"
#include "utils.h"
#include "matrix.h"
#include "asnf.h"
#include "mm.h"

int main(int argc, char *argv[])
{
	char *filename = NULL;
	char *dimension = NULL;
    entry_t l[] = { 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0};
	matrix_t *mat = from_list_matrix(l,3,4);
	matrix_t *cp = NULL;
	matrix_t *rt = NULL, *ct = NULL;
	matrix_t *m1 = NULL, *m2 = NULL;
	long ntime = 0, mtime = 0;
	pid_t pid = getpid();
	int nloops=1, opt, show_time = 0, verbose = 0;
	int m=1600,n=6400;

	use_log = 0;
    omp_set_num_threads(NUM_THREADS);

	while ((opt = getopt(argc, argv, "vcmtln:f:pd:j:")) != -1) {
		switch (opt) {
        case 'j':
            omp_set_num_threads(atoi(optarg));
		case 'd':
			dimension = optarg;
			if (sscanf(dimension, "%dx%d", &m,&n)==EOF) {
				fprintf(stderr, "dimension must be in the form mxn");
				exit(1);
			}
			break;
		case 'v':
			verbose = 1;
			break;
		case 't':
			show_time = 1;
			break;
		case 'l':
			use_log = 1;
			break;
		case 'n':
			nloops = atoi(optarg);
			if (nloops<0) {
				nloops = 1;
			}
			break;
		case 'f':
			filename = optarg;
			break;
		default: /* '?' */
			fprintf(stderr, "Usage: %s [-c] [-m] [-t] [-l] [-n loops] [-f name]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	if (verbose) {
		use_log = 1;
		show_time = 1;
	}

	for (dim_t i=0; i<nloops; i++) {
		flog("loop %lu: g", i);

        if (filename) {
		    mat = read_from_file_matrix(filename);
        }
		flog("f ");
		if (!is_allocated(mat)) {
			flog("\b\br ");
			mat	= random_matrix(m,n);
		}
		flog("c ");
		cp = structural_copy_mat(mat);
		rt = identity_matrix(mat->nrows);
        ct = identity_matrix(mat->ncols);
        flog("asnf ");
		tic();
		almost_smith_coltrans(mat, rt, ct);
		ntime += toc();

        flog("m ");
        tic();
        m1 = multiply_matrices(rt, cp);
        m2 = multiply_matrices(m1, ct);
        mtime += toc();

        if (!equal_matrices(m2, mat)) {
            display_matrix_gap("cp", cp);
            display_matrix_gap("rt", rt);
            display_matrix_gap("ct", ct);
            display_matrix_gap("mat", mat);
            display_matrix_gap("m2", m2);
            i = nloops;
        }

		flog("f ");
		free_matrix(mat);
        mat = NULL;
		free_matrix(cp);
        cp = NULL;
		free_matrix(rt);
        rt = NULL;
        free_matrix(ct);
        ct = NULL;
        free_matrix(m1);
        m1 = NULL;
        free_matrix(m2);
        m2 = NULL;
		flog("\n");
	}
	if (show_time) {
		fprintf(stderr,"[%u] Time spent on nulls_new calc: %4ums (%ldns)\n", pid, (unsigned int)(ntime/1000000), ntime);
		fprintf(stderr,"[%u] Time spent on multiplication: %4ums (%ldns)\n", pid, (unsigned int)(mtime/1000000), mtime);
	}
	return 0;
}
