#include <stdio.h>
#include <stdlib.h>

#include "matrix.h"

inline void help(const char *name)
{
	fprintf(stderr, "Usage: %s -i input_filename -o output_filename [-j njobs]\n", name);
}

int main(int argc, char *argv[])
{
	char *inname = NULL, 
		*outname = NULL;
	int opt;
	matrix_t *result = NULL;
	matrices_t m, r;

	r.num = 1;
	r.mats = (matrix_t**)malloc(sizeof(matrix_t*));

    omp_set_num_threads(NUM_THREADS);

	while ((opt = getopt(argc, argv, "hi:o:j:")) != -1) {
		switch (opt) {
		case 'i':
			inname = optarg;
			break;
		case 'o':
			outname = optarg;
			break;
        case 'j':
            omp_set_num_threads(atoi(optarg));
			break;
		case 'h':
			help(argv[0]);
			exit(EXIT_SUCCESS);
		default: /* '?' */
			help(argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	if (!(inname && outname)) {
		help(argv[0]);
		exit(EXIT_FAILURE);
	}
	if ( !read_from_file_matrices(inname, &m) ) {
		fprintf(stderr, "Can't read input file %s\n", inname);
		exit(EXIT_FAILURE);
	}
	if (m.num<2) {
		fprintf(stderr, "input file must contain two matrices\n");
		free_matrices(&m);
		exit(EXIT_FAILURE);
	}
	result = multiply_matrices(m.mats[0], m.mats[1]);
	if (!result) {
		fprintf(stderr, "cannot multiply %lux%lu by %lux%lu\n", m.mats[0]->nrows, m.mats[0]->ncols, m.mats[1]->nrows, m.mats[1]->ncols);
		free_matrices(&m);
		exit(EXIT_FAILURE);
	}
	r.mats[0] = result;
	write_to_file_matrices(outname, r.mats, r.num);
	free_matrices(&m);
	free_matrices(&r);

	return 0;
}
