#include <stdio.h>
#include <stdlib.h>

#include "asnf.h"

inline void help(const char *name)
{
	fprintf(stderr, "Usage: %s -i input_filename -m modulus -o output_filename [-j njobs]\n", name);
}

int main(int argc, char *argv[])
{
	char *inname = NULL, 
		*outname = NULL;
	int opt = 1;
    entry_t mod = 3;

	matrices_t m, r;

	r.num = 1;
	r.mats = (matrix_t**)malloc(sizeof(matrix_t*));

    omp_set_num_threads(NUM_THREADS);

	while ((opt = getopt(argc, argv, "hi:o:j:m:")) != -1) {
		switch (opt) {
		case 'i':
			inname = optarg;
			break;
        case 'm':
            mod = atoi(optarg);
            break;
		case 'o':
			outname = optarg;
			break;
        case 'j':
            omp_set_num_threads(atoi(optarg));
			break;
		case 'h':
			help(argv[0]);
			exit(EXIT_SUCCESS);
		default: /* '?' */
			help(argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	if ( !(inname && outname && ppart(mod)==mod) ) {
		help(argv[0]);
		exit(EXIT_FAILURE);
	}
	if ( !read_from_file_matrices(inname, &m) ) {
		fprintf(stderr, "Can't read input file %s\n", inname);
		exit(EXIT_FAILURE);
	}
	if (m.num<1) {
		fprintf(stderr, "input file must contain one matrix\n");
		free_matrices(&m);
		exit(EXIT_FAILURE);
	}
    r.mats[0] = nullspace_mat_mod_n( m.mats[0], mod );
	if (! r.mats[0] ) {
		fprintf(stderr, "cannot compute nullspace of %lux%lu matrix mod %u\n", m.mats[0]->nrows, m.mats[0]->ncols, mod );
		free_matrices(&r);
        free_matrices(&m);
		exit(EXIT_FAILURE);
	}
	write_to_file_matrices(outname, r.mats, r.num);
	free_matrices(&m);
	free_matrices(&r);

	return 0;
}
