#ifndef SMITH_ASNF_H
#define SMITH_ASNF_H

#include "common.h"
#include "utils.h"
#include "matrix.h"

dim_t find_pivot_in_row(matrix_t *mat, dim_t row, dim_t offset);

dim_t find_pivot_in_col(matrix_t *mat, dim_t col, dim_t offset);

ind_t find_pivot(matrix_t *mat, dim_t offset);

void swap_rows_two_mats(matrix_t *m1, matrix_t *m2, dim_t k, dim_t l);

void clear_column(matrix_t *mat, matrix_t *rts, dim_t offset);

void clear_column_simple(matrix_t *mat, matrix_t *rts, dim_t offset);

void almost_smith_coltrans(matrix_t *mat, matrix_t *rowtrans, matrix_t *coltrans);

void almost_smith(matrix_t *mat, matrix_t *rowtrans);

matrix_t *nullspace_mat_mod_n(matrix_t *mat, entry_t mod);

#endif // SMITH_ASNF_H
