#ifndef SMITH_MM_H
#define SMITH_MM_H

#include "common.h"

#define min(a,b) ((a<b)?(a):(b)) 

void mm(const entry_t *A, const entry_t *B, entry_t *C, const dim_t ra, const dim_t ca, const dim_t cb);

#endif //SMITH_MM_H