#ifndef SMITH_UTILS_H
#define SMITH_UTILS_H

#include "common.h"

void tic(void);
long toc(void);

extern int use_log;

int flog(const char *fmt, ...);

extern int srand_done;

void random_init(void);
int rand_interval(const int a, const int b);

#endif // SMITH_UTILS_H
