#ifndef SMITH_MATRIX_H
#define SMITH_MATRIX_H

#include "common.h"
#include "utils.h"
#include "mm.h"

matrix_t *alloc_matrix(dim_t m, dim_t n);

void free_matrix(matrix_t *mat);
void free_matrices(matrices_t *m);

matrix_t *zero_matrix(dim_t m, dim_t n);

matrix_t *identity_matrix(dim_t n);

matrix_t *from_list_matrix(entry_t *list, dim_t m, dim_t n);

matrix_t *matrix_mod_n(matrix_t *mat, entry_t mod);

/*
 * M has n columns
 * k and l are correct row numbers
 */
void sr(entry_t *M, const dim_t n, const dim_t k, const dim_t l);
void swap_rows(matrix_t *mat, dim_t k, dim_t l);

/*
 * M is m by n
 * k and l are correct
 */
void sc(entry_t *M, const dim_t m, const dim_t n, const dim_t k, const dim_t l);
void swap_cols(matrix_t *mat, dim_t k, dim_t l);

void add_part_row(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset);

void add_part_row_block_16(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset);

void add_part_col(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset);

void add_part_col_new(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset);

void multiply_row(matrix_t *mat, dim_t row, entry_t num, dim_t offset);

void multiply_col(matrix_t *mat, dim_t col, entry_t num, dim_t offset);

size_t write_to_file_matrix(const char *filename, matrix_t *mat);

size_t write_to_file_matrices(const char *filename, matrix_t **mat, dim_t num);

matrix_t *read_from_file_matrix(const char *filename);

matrices_t *read_from_file_matrices(const char *filename, matrices_t *m);

void display_matrix(matrix_t *mat);

void display_matrix_gap(const char *varname, matrix_t *mat);

matrix_t *structural_copy_mat(matrix_t *mat);

matrix_t *multiply_matrices(const matrix_t *l, const matrix_t *r);

int equal_matrices(matrix_t *l, matrix_t *r);

int is_zero_mat_mod_n(matrix_t *mat, entry_t mod);

matrix_t *random_matrix(dim_t m, dim_t n);

#endif // SMITH_MATRIX_H
