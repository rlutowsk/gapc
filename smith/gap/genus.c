#include "src/compiled.h"

#include "matrix.h"
#include "asnf.h"

matrix_t *MATRIX_LISTOBJ(Obj A)
{
	dim_t m,n,i,j;
	matrix_t *mat;

	if (! (IS_PLIST(A) || IS_POSOBJ(A)) || LEN_PLIST(A) < 1 || 
		! (IS_PLIST(A) || IS_POSOBJ(ELM_PLIST(A, 1)))) {
		ErrorQuit("A must be an integer matrix",0L,0L); 
	}
	m = (unsigned long) LEN_PLIST(A);
	n = (unsigned long) LEN_PLIST(ELM_PLIST(A, 1));

	mat = alloc_matrix(m,n);
	if (!mat) {
		ErrorQuit("Error in matrix allocation", 0L, 0L);
	}
	for (i=0; i<m;i++) {
		for (j=0;j<n;j++) {
			mat->list[i*n+j] = (entry_t)INT_INTOBJ(ELM_PLIST(ELM_PLIST(A,i+1),j+1));
		}
	}
	return mat;
}

Obj LISTOBJ_MATRIX(matrix_t *mat)
{
	Obj A, row;
	dim_t m = mat->nrows,
		  n = mat->ncols,
		  i,j;

	A   = NEW_PLIST( T_PLIST_DENSE, m );
	row = NEW_PLIST( T_PLIST_DENSE, n );
	SET_LEN_PLIST( A  , mat->nrows );
	SET_LEN_PLIST( row, mat->ncols );
	CHANGED_BAG( A );
	CHANGED_BAG( row );
	for (i=0; i<m; i++) {
		for (j=0; j<n; j++) {
			SET_ELM_PLIST( row, j+1, INTOBJ_INT( mat->list[i*n+j] ) );
			CHANGED_BAG( row );
		}
		SET_ELM_PLIST( A, i+1, SHALLOW_COPY_OBJ( row ) );
		CHANGED_BAG( A );
	}
	return A;
}


Obj FuncMat2Mat( Obj self, Obj A )
{
	matrix_t *mat;
	Obj M;

	mat = MATRIX_LISTOBJ( A );
	M   = LISTOBJ_MATRIX(mat);
	free_matrix( mat );
	return M;
}

Obj FuncTestMM( Obj self, Obj A, Obj B )
{
	matrix_t *a, *b, *c;
	Obj C;

	a = MATRIX_LISTOBJ( A );
	b = MATRIX_LISTOBJ( B );

	c = multiply_matrices( a, b );

	C = LISTOBJ_MATRIX( c );

	free_matrix( a );
	free_matrix( b );
	free_matrix( c );

	return C;
}

Obj FuncTestASNF( Obj self, Obj A )
{
	matrix_t *mat = MATRIX_LISTOBJ( A ),
			 *row = identity_matrix( mat->nrows ),
			 *col = identity_matrix( mat->ncols );
	Obj rec;
	if ( !(row && col) ) {
		ErrorQuit("TestASNF: Error in matrix allocation", 0L, 0L);
	}
	almost_smith_coltrans(mat, row, col);

	rec = NEW_PREC( 3 );
	SET_RNAM_PREC( rec, 1, RNamName("normal")  );
	SET_ELM_PREC(  rec, 1, LISTOBJ_MATRIX( mat ) );
	SET_RNAM_PREC( rec, 2, RNamName("rowtrans"));
	SET_ELM_PREC(  rec, 2, LISTOBJ_MATRIX( row ) );
	SET_RNAM_PREC( rec, 3, RNamName("coltrans"));
	SET_ELM_PREC(  rec, 3, LISTOBJ_MATRIX( col ) );
	SET_LEN_PREC(  rec, 3 );
	CHANGED_BAG( rec );

	free_matrix( mat );
	free_matrix( row );
	free_matrix( col );

	return rec;
}

Obj FuncTic( Obj self )
{
	tic();
	return True;
}
Obj FuncToc( Obj self )
{
	return INTOBJ_INT( toc() );
}

Obj FuncSetNumThreads( Obj self, Obj N )
{
	int num = INT_INTOBJ( N );
	printf("%d\n", num);
	if (num<1) {
		num = 1;
	}
	if (num > 2*CPU_COUNT) {
		num = 2*CPU_COUNT;
	}
	num_threads = num;
	return INTOBJ_INT( num );
}

Obj FuncGetNumThreads( Obj self )
{
	return INTOBJ_INT( num_threads );
}

Obj FuncRandomMatrix( Obj self, Obj m, Obj n )
{
    matrix_t *mat = random_matrix( INT_INTOBJ(m), INT_INTOBJ(n) );
    Obj M;
    if ( !mat ) {
        ErrorQuit("Can't generate random matrix", 0L, 0L);
    }
    M = LISTOBJ_MATRIX( mat );
    free_matrix( mat );
    return M;
}

Obj FuncWriteToFileMatrices( Obj self, Obj filename, Obj list)
{
	matrix_t **mats;
	size_t nb;
	int i;
	if ( !IS_STRING_REP(filename) ) {
		ErrorQuit("filename must be a string", 0L, 0L);
	}
	if ( !(IS_PLIST(list) && LEN_PLIST(list)>0) ) {
		ErrorQuit("list must be a list of matrices", 0L, 0L);
	}
	mats = (matrix_t**)malloc(LEN_PLIST(list)*sizeof(matrix_t*));
	if (!mats) {
		ErrorQuit("Can't allocate memory for mats", 0L, 0L);
	}
	for (i=0; i<LEN_PLIST(list); i++) {
		mats[i] = MATRIX_LISTOBJ(ELM_PLIST(list, i+1));
	}
	nb = write_to_file_matrices(CSTR_STRING(filename), mats, LEN_PLIST(list));
	for (i=0; i<LEN_PLIST(list); i++) {
		free_matrix(mats[i]);
	} 
	free(mats);
	return INTOBJ_INT(nb);
}

Obj FuncReadFromFileMatrices( Obj self, Obj filename )
{
	Obj list;
	matrices_t m;
	dim_t i;
	if (!read_from_file_matrices(CSTR_STRING(filename), &m)) {
		ErrorQuit("Can't read matrices from file %s", (Int)CSTR_STRING(filename), 0L);
	}
	list = NEW_PLIST( T_PLIST_DENSE, m.num );
	SET_LEN_PLIST( list, m.num );
	CHANGED_BAG( list );
	for (i=0; i<m.num; i++) {
		SET_ELM_PLIST( list, i+1, LISTOBJ_MATRIX(m.mats[i]) );
		CHANGED_BAG( list );
	}
	return list;
}

/*F * * * * * * * * * * * * * initialize package * * * * * * * * * * * * * * *
*/



/****************************************************************************
**

*V  GVarFilts . . . . . . . . . . . . . . . . . . . list of filters to export
*/
/*static StructGVarFilt GVarFilts [] = {

    { "IS_BOOL", "obj", &IsBoolFilt,
      IsBoolHandler, "src/bool.c:IS_BOOL" },

    { 0 }

};   ?????*/

/****************************************************************************
**

*V  GVarFuncs . . . . . . . . . . . . . . . . . . list of functions to export
*/
static StructGVarFunc GVarFuncs [] = {

	{ "TestMat2Mat"         , 1, "A"             , FuncMat2Mat             , "genus.c:TestMat2Mat"         },
	{ "TestMM"              , 2, "A, B"          , FuncTestMM              , "genus.c:TestMM"              },
	{ "TestASNF"            , 1, "A"             , FuncTestASNF            , "genus.c:TestASNF"            },
	{ "Tic"                 , 0, ""              , FuncTic                 , "genus.c:Tic"                 },
	{ "Toc"                 , 0, ""              , FuncToc                 , "genus.c:Toc"                 },
    { "RandomMatrix"        , 2, "m, n"          , FuncRandomMatrix        , "genus.c:RandomMatrix"        },
    { "SetNumThreads"       , 1, "N"             , FuncSetNumThreads       , "genus.c:SetNumThreads"       },
	{ "GetNumThreads"       , 0, ""              , FuncGetNumThreads       , "genus.c:GetNumThreads"       },
	{ "WriteToFileMatrices" , 2, "filename, list", FuncWriteToFileMatrices , "genus.c:WriteToFileMatrices" },
	{ "ReadFromFileMatrices", 1, "filename"      , FuncReadFromFileMatrices, "genus.c:ReadFromFileMatrices"},
	{ 0 }

};



/**************************************************************************

*F  InitKernel( <module> )  . . . . . . . . initialise kernel data structures
*/
static Int InitKernel (
    StructInitInfo *    module )
{

    /* init filters and functions                                          */
    InitHdlrFuncsFromTable( GVarFuncs );

    /* return success                                                      */
    return 0;
}


/****************************************************************************
**
*F  InitLibrary( <module> ) . . . . . . .  initialise library data structures
*/
static Int InitLibrary (
    StructInitInfo *    module )
{
  /*    UInt            gvar;
	Obj             tmp; */

    /* init filters and functions                                          */
    /* printf("Init El..Small\n");fflush(stdout); */
    InitGVarFuncsFromTable( GVarFuncs );

    /* return success                                                      */
    return 0;
}


/******************************************************************************
*F  InitInfopl()  . . . . . . . . . . . . . . . . . table of init functions
*/
/* <name> returns the description of this module */

static StructInitInfo module = {
    .type = MODULE_DYNAMIC,
    .name = "genus",
    .initKernel = InitKernel,
    .initLibrary = InitLibrary,
};

StructInitInfo * Init__Dynamic ( void )
{
 return &module;
}

StructInitInfo * Init__genus ( void )
{
  return &module;
}

