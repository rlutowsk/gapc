if not IsBound( ASNF_NUM_CPUS ) then
	BindGlobal( "ASNF_NUM_CPUS", @CPU_COUNT@ );
fi;
ASNF_NUM_THREADS := ASNF_NUM_CPUS;

AsnfSetNumThreads := function( num )
	local n;
	n := num;
	if num < 1 then
		n := 1;
	fi;
	if num > ASNF_NUM_CPUS then
		n := ASNF_NUM_CPUS;
	fi;
	ASNF_NUM_THREADS := n;
	return n;
end;

if not IsBound( ASNF_ROOT_DIR ) then
	BindGlobal( "ASNF_ROOT_DIR", Directory( "@ROOTDIR@" ) );
fi;
if not IsBound( ASNF_BIN_DIR ) then
	BindGlobal( "ASNF_BIN_DIR" , Directory( "@ROOTDIR@/bin" ) );
fi;
if not IsBound( ASNF_TMP_DIR ) then
	BindGlobal( "ASNF_TMP_DIR" , DirectoryTemporary() );
fi;

if not IsBound(TestMM) then
	LoadDynamicModule( Filename(ASNF_BIN_DIR, "genus.so") );
fi;

AsnfTmpDir := function()
    # if Carat temporary directory has disappeared, recreate it
    if not IsDirectoryPath( ASNF_TMP_DIR![1] ) then
        MakeReadWriteGlobal( "ASNF_TMP_DIR" );
        UnbindGlobal( "ASNF_TMP_DIR" );
        BindGlobal( "ASNF_TMP_DIR", DirectoryTemporary() );
    fi;
    return ASNF_TMP_DIR;
end;

AsnfCommand := function( dir, command, args )
  local cmd, err;
  cmd := Filename( ASNF_BIN_DIR, command );
  if cmd = fail then
	Error(Concatenation( "ASNF command ", cmd, " not found"));
  fi;
  err := Process( dir, cmd, InputTextNone(), OutputTextNone(), args );
  if err <> 0 then
	Error(Concatenation("ASNF command ", cmd, " returned an error code"));
  fi;
end;

AsnfMM := function( a, b )
	local tmp;
	tmp := AsnfTmpDir();
	WriteToFileMatrices( Filename( tmp, "in" ), [a,b] );
	AsnfCommand( tmp, "mm", ["-i", "in", "-o", "out", "-j", String(ASNF_NUM_THREADS)] );
	return ReadFromFileMatrices( Filename( tmp, "out" ) )[1];
end;

AsnfNFRC := function( a )
	local tmp, r;
	tmp := AsnfTmpDir();
	WriteToFileMatrices( Filename( tmp, "in" ), [a] );
	AsnfCommand( tmp, "asnfrc", ["-i", "in", "-o", "out", "-j", String(ASNF_NUM_THREADS)] );
	r := ReadFromFileMatrices( Filename( tmp, "out" ) );
	return rec( normal := r[1], rowtrans := r[2], coltrans := r[3] );
end;
