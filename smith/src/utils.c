#include "utils.h"

struct timespec tv;

void tic(void)
{
	clock_gettime(CLOCK_REALTIME, &tv);
}
long toc(void)
{
	static struct timespec st;
	clock_gettime(CLOCK_REALTIME, &st);
	return (tv.tv_sec==st.tv_sec) ? st.tv_nsec - tv.tv_nsec : 1000000000*(st.tv_sec-tv.tv_sec)+st.tv_nsec - tv.tv_nsec;
}

int use_log = 1;

int flog(const char *fmt, ...)
{
	int n = -1;
	va_list args;
	if (use_log) {
		va_start(args, fmt);
		n = vfprintf(stderr, fmt, args);
		va_end(args);
	}
	return n;
}

int srand_done = 0;

void random_init(void)
{
	if (srand_done) {
		return;
	}
	struct timeval t = {0,0};
	gettimeofday(&t,NULL);
	srand(t.tv_sec*1000000+t.tv_usec);
	srand_done = 1;
}

inline int rand_interval(const int a, const int b)
{
    return rand()%(b-a)+a;
}
