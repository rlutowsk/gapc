#include "asnf.h"

/* inner functions: */

/*
 * M has n columns
 * R has m columns
 * assume k and l are less than number of rows in M and R
 */
void srtm(entry_t *M, entry_t *R, const dim_t n, const dim_t m, const dim_t k, const dim_t l);

dim_t find_pivot_in_row(matrix_t *mat, dim_t row, dim_t offset)
{
	dim_t i, p = offset;
	entry_t	*e = &(mat->list[row*mat->ncols+offset]);
	entry_t min = ppart(*e),
			pp;
	for (i=offset; i<mat->ncols;i++,e++) {
		if (*e) {
			pp = ppart(*e);
			if (pp == 1) {
				return i;
			}
			if ( !min || pp < min ) {
				min = pp;
				p	 = i;
			}
		}
	}
	return p;
}

dim_t find_pivot_in_col(matrix_t *mat, dim_t col, dim_t offset)
{
	dim_t	i, p = offset;
	entry_t	*e = &(mat->list[offset*mat->ncols+col]);
	entry_t min = ppart(*e),
			pp;
	for (i=offset; i<mat->nrows;i++, e+=(mat->ncols)) {
		if (*e) {
			pp = ppart(*e);
			if (pp == 1) {
				return i;
			}
			if ( !min || pp < min ) {
				min = pp;
				p	 = i;
			}
		}
	}
	return p;
}

ind_t find_pivot(matrix_t *mat, dim_t offset)
{
	ind_t old, 
		  new = {offset, offset}; // returning values of pivots
	do {
		old.row = new.row;
		old.col = new.col;
		new.col = find_pivot_in_row(mat, old.row, offset);
		new.row = (ppart(mat->rows[new.row][new.col])==1) ? new.row : find_pivot_in_col(mat, new.col, offset);
	} while ( ppart(mat->rows[new.row][new.col])!=1 && (old.row!=new.row || old.col!=new.col) );
	return new;
}

/* omp here is a lost */
void srtm(entry_t *M, entry_t *R, const dim_t n, const dim_t m, const dim_t k, const dim_t l)
{
	dim_t   i;
	entry_t t;
	for (i=0;i<min(m,n);i++) {
		t = M[k*n+i];
		M[k*n+i] = M[l*n+i];
		M[l*n+i] = t;
		t = R[k*m+i];
		R[k*m+i] = R[l*m+i];
		R[l*m+i] = t;
	}
	if (m<n) {
		for (;i<n;i++) {
			t = M[k*n+i];
			M[k*n+i] = M[l*n+i];
			M[l*n+i] = t;
		}
	} else {
		for (;i<m;i++) {
			t = R[k*m+i];
			R[k*m+i] = R[l*m+i];
			R[l*m+i] = t;
		}
	}
}

inline void swap_rows_two_mats(matrix_t *m1, matrix_t *m2, dim_t k, dim_t l)
{
	if ( k > m1->nrows || l > m1->nrows || k > m2->nrows || l > m2->nrows ) {
		return;
	}
	srtm(m1->list, m2->list, m1->ncols, m2->ncols, k, l);
}

void clear_column(matrix_t *mat, matrix_t *rts, dim_t offset)
{
	entry_t *M = mat->list, 
			*R = rts->list;
	dim_t i,r,m=rts->ncols,n=mat->ncols;
	entry_t diag = M[offset*n+offset], 
					shift, aj;
	entry_t *cache = (entry_t*)malloc(m*sizeof(entry_t));

	if (!diag) {
		return;
	};
	shift = pshift(diag);

	#pragma omp parallel for private (r,i,aj) shared (M)
	for (r=offset+1; r<m; r++) {
		/* matrix */
		aj = -(M[r*n+offset] >> shift);
		cache[r] = aj;
		if (!aj) {
			//zeroes++;
			continue;
		}
		for (i=offset; i<n; i++) {
		/* mat transforms */
			M[r*n+i] += aj * M[offset*n+i];
		}
	}
	#pragma omp parallel for private (r,i,aj) shared (R)
	for (r=offset+1; r<m; r++) {
		/* matrix */
		aj = cache[r];
		if (!aj) {
			//zeroes++;
			continue;
		}
		for (i=0; i<m; i++) {
			R[r*m+i] += aj * R[offset*m+i];
		}
	}
	free(cache);
}

void almost_smith_coltrans(matrix_t *mat, matrix_t *rowtrans, matrix_t *coltrans)
{
	dim_t max = (mat->nrows < mat->ncols) ? mat->nrows : mat->ncols,
			offset;//, i;
	ind_t pivot;
	entry_t diag, inv;//, elm;
	
	omp_set_num_threads( num_threads );

	for ( offset=0; offset<max; offset++ ) {
		pivot = find_pivot(mat, offset);
		if (pivot.row != offset) {
			swap_rows_two_mats(mat, rowtrans, offset, pivot.row);
		}
		if (pivot.col != offset) {
			swap_cols(mat, offset, pivot.col);
			swap_cols(coltrans, offset, pivot.col);
		}
		diag = entry(mat,offset,offset);
		if ( !diag ) {
			continue;
		}
		inv = pinv(diag);
		if ( inv != 1 ) {
			multiply_row(mat, offset, inv, offset);
			diag = entry(mat,offset,offset);
			multiply_row(rowtrans, offset, inv, 0);
		}
		/* assume that mat[offset][offset] is nonzero */
	    clear_column(mat, rowtrans, offset);
	}
}

void almost_smith(matrix_t *mat, matrix_t *rowtrans)
{
	dim_t max = (mat->nrows < mat->ncols) ? mat->nrows : mat->ncols,
			offset;//, i;
	ind_t pivot;
	entry_t diag, inv;//, elm;

	omp_set_num_threads( num_threads );

	for ( offset=0; offset<max; offset++ ) {
		pivot = find_pivot(mat, offset);
		if (pivot.row != offset) {
			swap_rows_two_mats(mat, rowtrans, offset, pivot.row);
		}
		if (pivot.col != offset) {
			swap_cols(mat, offset, pivot.col);
		}
		diag = entry(mat,offset,offset);
		if ( !diag ) {
			continue;
		}
		inv = pinv(diag);
		if ( inv != 1 ) {
			multiply_row(mat, offset, inv, offset);
			diag = entry(mat,offset,offset);
			multiply_row(rowtrans, offset, inv, 0);
		}
		/* assume that mat[offset][offset] is nonzero */
	    clear_column(mat, rowtrans, offset);
	}
}

matrix_t *nullspace_mat_mod_n(matrix_t *mat, entry_t mod)
{
	matrix_t *ns = NULL;
	entry_t mask = mod-1,
			elm, mult,
			*e;
	dim_t i,j,dim;

	if ( ppart(mod)!=mod ) {
		return NULL;
	}
	dim = mat->nrows;
	ns	= identity_matrix( dim );
	if ( !ns ) {
		return NULL;
	}
	almost_smith(mat, ns);
	for (i=0; i<min(mat->nrows,mat->ncols); i++) {
		elm = mask & entry(mat,i,i);
		e	 = ns->rows[i];
		if (elm==1) {
			//for (j=0; j<dim; ns->rows[i][j++] = 0);
			for (j=0; j<dim; *(e++) = 0, j++);
		} else {
			for (j=0; j<dim; j++) {
				*(e++) &= mask;
			}
			if (elm) {
				mult = mod / elm;
				e	= ns->rows[i];
				for (j=0; j<dim; j++) {
					*(e++) *= mult;
				}
			}
		}
	}
	return ns;
}
