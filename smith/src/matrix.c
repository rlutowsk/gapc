#include "matrix.h"

/* be careful here - it should be zero matrix at start, but ... */
matrix_t *alloc_matrix(dim_t m, dim_t n) 
{
	matrix_t *mat;
	dim_t i;
	if (m==0 || n==0) {
		return NULL;
	}
	mat = (matrix_t*)malloc(sizeof(matrix_t));
	if (!mat) {
		return NULL;
	}
	mat->list = (entry_t*)malloc(m*n*sizeof(entry_t));
	if ( !(mat->list) ) {
		free(mat);
		return NULL;
	}
	mat->rows = (entry_t**)malloc(m*sizeof(entry_t*));
	if (!(mat->rows)) {
		free(mat->list);
		free(mat);
		return NULL;
	}
	mat->auxc = (entry_t*)malloc(m*sizeof(entry_t));
	if ( !(mat->auxc) ) {
		free(mat->rows);
		free(mat->list);
		free(mat);
		return NULL;
	}
	for (i=0; i<m; i++) {
		mat->rows[i] = &(mat->list[i*n]);
	}
	mat->nrows = m;
	mat->ncols = n;
	return mat;
}

void free_matrix(matrix_t *mat)
{
	if (!is_allocated(mat)) {
		return;
	} 
	free(mat->rows);
	free(mat->list);
	free(mat->auxc);
	mat->nrows = 0;
	mat->ncols = 0;
	free(mat);
	mat = NULL;
}

void free_matrices(matrices_t *m)
{
	for (int i=0; i<m->num; i++) {
		free_matrix(m->mats[i]);
	}
}

matrix_t *zero_matrix(dim_t m, dim_t n)
{
	matrix_t *mat = alloc_matrix(m,n);
	if (mat) {
		bzero(mat->list, m*n);
	}
	return mat;
}

matrix_t *identity_matrix(dim_t n)
{
	dim_t i;
	matrix_t *mat = zero_matrix(n,n);
	if ( mat ) {
		for (i=0; i<n; i++) {
			mat->list[n*i+i] = 1;
		}
	}
	return mat;
}

matrix_t *from_list_matrix(entry_t *list, dim_t m, dim_t n)
{
	dim_t i, size;
	matrix_t *mat = alloc_matrix(m,n);
	if (!mat) {
		return NULL;
	}
	size = m*n;
	for (i=0; i<size; i++) {
		mat->list[i] = list[i];
	}
	return mat;
}

matrix_t *matrix_mod_n(matrix_t *mat, entry_t mod) 
{
	dim_t i, len = mat->nrows * mat->ncols;
	entry_t mask = mod-1,
				*e = mat->list;
	if ( ppart(mod)!=mod ) {
		return NULL;
	}
	for (i=0; i<len; i++,e++) {
		*e &= mask;
	}
	return mat;
}

void sr(entry_t *M, const dim_t n, const dim_t k, const dim_t l)
{
	dim_t   i;
	entry_t t;

	#pragma omp parallel for private (i,t) shared (M)
	for (i=0; i<n; i++) {
		       t = M[k*n+i];
		M[k*n+i] = M[l*n+i];
		M[l*n+i] = t;
	}
}

inline void swap_rows(matrix_t *mat, dim_t k, dim_t l) 
{
	if ( k > mat->nrows || l > mat->nrows ) {
		return;
	}
	sr(mat->list, mat->ncols, k, l);
}

void sc(entry_t *M, const dim_t m, const dim_t n, const dim_t k, const dim_t l)
{
	dim_t   i;
	entry_t t;

	#pragma omp parallel for private (i,t) shared (M)
	for (i=0;i<m;i++) {
		       t = M[i*n+k];
		M[i*n+k] = M[i*n+l];
		M[i*n+l] = t;
	}
}

inline void swap_cols(matrix_t *mat, dim_t k, dim_t l)
{
	if ( k > mat->ncols || l > mat->ncols ) {
		return;
	}
	sc(mat->list, mat->nrows, mat->ncols, k, l);
}

void add_part_row(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset)
{
	/*dim_t i;
	for (i=offset; i<mat->ncols; i++) {
		mat->rows[dst][i] += mult * mat->rows[src][i];
	}*/
	/* the below is faster */ 
	dim_t s,d,i;
	d = dst*mat->ncols + offset;
	s = src*mat->ncols + offset;
	for ( i=offset; i<mat->ncols; i++,d++,s++ ) {
		mat->list[d] += mult * mat->list[s];
	}
}

// TODO: try to unroll as in matrix multiplication example

void add_part_row_block_16(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset)
{
	/*
	dim_t i;
	for (i=offset; i<mat->ncols; i++) {
		mat->rows[dst][i] += mult * mat->rows[src][i];
	}
	*/
	/* the below is faster */
	dim_t s,d,i;
		offset -= (offset%16);
	d = dst*mat->ncols + offset;
	s = src*mat->ncols + offset;
	for ( i=offset; i<mat->ncols; i+=16,d+=16,s+=16 ) {
		mat->list[d	] += mult * mat->list[s	];
				mat->list[d+1] += mult * mat->list[s+1];
				mat->list[d+2] += mult * mat->list[s+2];
				mat->list[d+3] += mult * mat->list[s+3];
				mat->list[d+4] += mult * mat->list[s+4];
				mat->list[d+5] += mult * mat->list[s+5];
				mat->list[d+6] += mult * mat->list[s+6];
				mat->list[d+7] += mult * mat->list[s+7];
				mat->list[d+8] += mult * mat->list[s+8];
				mat->list[d+9] += mult * mat->list[s+9];
				mat->list[d+10] += mult * mat->list[s+10];
				mat->list[d+11] += mult * mat->list[s+11];
				mat->list[d+12] += mult * mat->list[s+12];
				mat->list[d+13] += mult * mat->list[s+13];
				mat->list[d+14] += mult * mat->list[s+14];
				mat->list[d+15] += mult * mat->list[s+15];
	}
}

void add_part_col(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset)
{
	dim_t s,d,i;
	d = offset*mat->ncols + dst;
	s = offset*mat->ncols + src;
	for ( i=offset; i<mat->nrows; i++ ) {
		mat->list[d] += mult * mat->list[s];
		d += mat->ncols;
		s += mat->ncols;
	}
}

void add_part_col_new(matrix_t *mat, dim_t dst, dim_t src, entry_t mult, dim_t offset)
{
	dim_t s,d,i;
	d = offset*mat->ncols + dst;
	s = offset*mat->ncols + src;
	for ( i=offset; i<mat->nrows; i++ ) {
		mat->list[d] += mult * mat->list[s];
		d += mat->ncols;
		s += mat->ncols;
	}
}

void mr(const entry_t num, entry_t *M, const dim_t n, const dim_t k, const dim_t offset)
{
	dim_t i;
	for (i=offset;i<n;i++) {
		M[k*n+i] *= num;
	}
}

inline void multiply_row(matrix_t *mat, dim_t row, entry_t num, dim_t offset)
{
	mr(num, mat->list, mat->ncols, row, offset);
}

void multiply_col(matrix_t *mat, dim_t col, entry_t num, dim_t offset)
{
	dim_t i;
	entry_t	*e = &(mat->list[offset*mat->ncols+col]);
	for (i=offset; i<mat->nrows;i++, e+=(mat->ncols)) {
		*e *= num;
	}
}

size_t write_to_file_matrix(const char *filename, matrix_t *mat)
{
	size_t bw;
	FILE *file = fopen(filename, "wb");
	bw = (size_t)fprintf(file, "%lux%lu\n", mat->nrows, mat->ncols);
	bw += fwrite(mat->list, 1, mat->nrows*mat->ncols, file);
	fclose(file);
	return bw;
}

size_t write_to_file_matrices(const char *filename, matrix_t **mat, dim_t num)
{
	size_t bw = 0;
	FILE *file = fopen(filename, "wb");
	if (!file) {
		return 0;
	}
	fprintf(file, "%lu\n", num);
	for (dim_t i=0; i<num; i++) {
		fprintf(file, "%lux%lu\n", mat[i]->nrows, mat[i]->ncols);
		bw += fwrite(mat[i]->list, 1, mat[i]->nrows*mat[i]->ncols, file);
	}
	fclose(file);
	return bw;
}

matrix_t *read_from_file_matrix(const char *filename)
{
	matrix_t *mat = NULL;
	dim_t nrows, ncols;
	size_t nb;
	FILE *file;
	file = fopen(filename, "rb");
	if (!file) {
		return NULL;
	}
	nb = (size_t)fscanf(file, "%lux%lu\n", &nrows, &ncols);
	mat = alloc_matrix(nrows, ncols);
	if (mat) {
		nb =fread(mat->list, 1, nrows*ncols, file);
		if (nb != nrows*ncols) {
			free_matrix(mat);
			mat = NULL;
		}
	}
	fclose(file);
	return mat;
}

matrix_t *from_stream_matrix(FILE *file)
{
	matrix_t *mat = NULL;
	dim_t nrows, ncols;
	size_t nb;
	
	if (!file) {
		return NULL;
	}
	nb = (size_t)fscanf(file, "%lux%lu\n", &nrows, &ncols);
	mat = alloc_matrix(nrows, ncols);
	if (mat) {
		nb = fread(mat->list, 1, nrows*ncols, file);
		if (nb != nrows*ncols) {
			free_matrix(mat);
			mat = NULL;
		}
	}

	return mat;
}

matrices_t *read_from_file_matrices(const char *filename, matrices_t *m)
{
	matrix_t **mat = NULL;
	dim_t num;
	size_t nb;

	FILE *file;
	file = fopen(filename, "rb");
	if (!file) {
		return NULL;
	}
	nb = fscanf(file, "%lu\n", &num);
	if (nb<=0) {
		fclose(file);
		return NULL;
	}
	mat = (matrix_t**)malloc(num*sizeof(matrix_t*));
	if (!mat) {
		fclose(file);
		return NULL;
	}
	for (dim_t i=0; i<num; i++) {
		mat[i] = from_stream_matrix(file);
		if (!mat[i]) {
			for (dim_t j=0; j<i; j++) {
				free_matrix(mat[i]);
			}
			free(mat);
			fclose(file);
			return NULL;
		}
	}
	fclose(file);
	m->num  = num;
	m->mats = mat;
	return m;
}

void display_matrix(matrix_t *mat)
{
	dim_t i,j,max=0;
	int l;
	if (!is_allocated(mat)) {
		return;
	}
	for (i=0; i<mat->nrows*mat->ncols; i++) {
		if (max < mat->list[i]) {
			max = mat->list[i];
		}
	}
	if (max < 10) {
		l = 1;
	} else if (max < 100) {
		l = 2;
	} else {
		l = 3;
	}
	for (i=0; i<mat->nrows; i++) {
		printf("[");
		for (j=0; j<mat->ncols; j++) {
			printf("%*u, ", l, mat->rows[i][j]);
		}
		printf("\b\b]\n");
	}
}

void display_matrix_gap(const char *varname, matrix_t *mat)
{
	dim_t i,j;
	if (!is_allocated(mat)) {
		return;
	}
	if (varname) {
		printf("%s:=", varname);
	}
	printf("[");
	for (i=0; i<mat->nrows; i++) {
		printf("[");
		for (j=0; j<mat->ncols; j++) {
			printf("%u,", mat->rows[i][j]);
		}
		printf("\b],");
	}
	printf("\b];\n");
}

matrix_t *structural_copy_mat(matrix_t *mat)
{
	matrix_t *copy = alloc_matrix(mat->nrows, mat->ncols);
	//dim_t i, len = mat->ncols * mat->nrows;
	//for (i=0; i<len; i++) {
	//	copy->list[i] = mat->list[i];
	//}
	memcpy(copy->list, mat->list, mat->ncols * mat->nrows);
	return copy;
}

matrix_t *multiply_matrices(const matrix_t *l, const matrix_t *r)
{
		//dim_t i,j,k;
		//long time = 0;
		matrix_t *result = NULL;
		if (l->ncols != r->nrows) {
				return NULL;
		}
		result = zero_matrix(l->nrows, r->ncols);
		if (!result) {
			return NULL;
		}
		mm(l->list, r->list, result->list, l->nrows, l->ncols, r->ncols);
		return result;
}

int equal_matrices(matrix_t *l, matrix_t *r)
{
	if (l->nrows!=r->nrows || l->ncols!=r->ncols) {
		return 0;
	}
	return (memcmp(l->list,r->list, l->nrows*l->ncols)==0);
}

int is_zero_mat_mod_n(matrix_t *mat, entry_t mod)
{
	entry_t mask = mod-1;
	dim_t	 i,
			len = mat->nrows * mat->ncols;
	if ( ppart(mod)!=mod ) {
		return -1;
	}
	for (i=0; i<len; i++) {
		if ( (mat->list[i] & mask) ) {
			return 0;
		}
	}
	return 1;
}

matrix_t *random_matrix(dim_t m, dim_t n)
{
	matrix_t *mat = alloc_matrix(m,n);
	dim_t	 len = m*n;
	if (!mat) {
		return NULL;
	}
	random_init();
	for (dim_t i=0; i<len; i++) {
		mat->list[i] = (entry_t)rand();
	}
	return mat;
}
