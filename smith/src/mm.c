#include "mm.h"

void mm(const entry_t *A, const entry_t *B, entry_t *C, const dim_t ra, const dim_t ca, const dim_t cb)
{
    dim_t i, j, k;
    entry_t ak;

	omp_set_num_threads( num_threads );

    #pragma omp parallel for private (i,j,k,ak) shared (A,B,C)
    for (i=0; i<ra; i++) {
        for (k=0; k<ca; k++) {
            ak = A[ca*i+k];
            for (j=0; j<cb; j++) {
                C[cb*i+j] += ak*B[cb*k+j];
            }
        }
    }
}

/*
void mm_reorder_unroll_2(entry_t *A, entry_t *B, entry_t *C, const dim_t ra, const dim_t ca, const dim_t cb)
{
    dim_t i, j, k;
    entry_t ak;
    bzero(C, ra*cb);
    for (i=0; i<ra; i++) {
        for (k=0; k<ca; k++) {
            ak = A[ca*i+k];
            for (j=0; j<cb; j+=2) {
                C[cb*i+j  ] += ak*B[cb*k+j  ];
                C[cb*i+j+1] += ak*B[cb*k+j+1];
            }
        }
    }
}
*/
