#include "src/compiled.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>

char  name[NAME_MAX] = { 0 };
int               fd = -1;
void            *mem = NULL;
UInt            *cnt = NULL;
pthread_mutex_t *mut = NULL;
size_t          ISIZ, MSIZ, TSIZ;
#define INIT_SIZES \
ISIZ = sizeof(UInt); \
MSIZ = sizeof(pthread_mutex_t); \
TSIZ = ISIZ + MSIZ; \
memset( name, 0, NAME_MAX );

Obj FuncInitSharedCounter(Obj self, Obj str)
{
	//fprintf( stdout, "name[0] = %d\n", name[0] );
	if ( fd > 0 || ! IS_STRING( str ) || name[0] ) {
		return Fail;
	}
	INIT_SIZES;

	strncpy( name, CSTR_STRING( str ), NAME_MAX-1 );
	fd = shm_open( name, O_RDWR | O_CREAT | O_EXCL, 00600 );
	if ( fd < 0 ) {
		return Fail;
	}
	if ( ftruncate( fd, TSIZ ) != 0 ) {
		return Fail;
	}
	
	mem = mmap( NULL, TSIZ, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );
	if ( mem == MAP_FAILED ) {
		shm_unlink( name );
		name[0] = 0;
		return Fail;
	}
	cnt = (UInt*)mem;
	mut = (pthread_mutex_t*)(mem+ISIZ);

	*cnt = 0;
	pthread_mutex_init(mut, NULL);

	return True;
}

Obj FuncJoinSharedCounter(Obj self, Obj str)
{
	if ( fd > 0 || ! IS_STRING( str ) || name[0] ) {
		return Fail;
	}
	INIT_SIZES;
	
	strncpy( name, CSTR_STRING( str ), NAME_MAX-1 );
	fd = shm_open( CSTR_STRING( str ), O_RDWR, 00600 );
	if ( fd < 0 ) {
		return Fail;
	}
	
	mem = mmap( NULL, TSIZ, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );
	if ( mem == MAP_FAILED ) {
		shm_unlink( name );
		name[0] = 0;
		return Fail;
	}
	cnt = (UInt*)mem;
	mut = (pthread_mutex_t*)(mem+ISIZ);

	return True;
}

Obj FuncDestroySharedCounter( Obj self, Obj str )
{
	fd = -1;
	munmap( mem, TSIZ );
	shm_unlink( CSTR_STRING( str ) );
	mem = NULL;
	cnt = NULL;
	mut = NULL;
	name[0] = 0;

	return True;
}

Obj FuncNextSharedCounter(Obj self)
{
	if ( fd < 0 ) {
		return Fail;
	}
	pthread_mutex_lock( mut );
	*cnt = *cnt+1;
	pthread_mutex_unlock( mut );

	return INTOBJ_INT( *cnt );
}

Obj FuncClearSharedCounter( Obj self )
{
	if ( fd < 0 ) {
		return Fail;
	}
	pthread_mutex_lock( mut );
	*cnt = 0;
	pthread_mutex_unlock( mut );

	return True;
}

/* 
 * GVarFunc - list of functions to export
 */
static StructGVarFunc GVarFunc[] = {
    { "InitSharedCounter",    1, "str", FuncInitSharedCounter,    "shared.c:InitSharedCounter" },
	{ "JoinSharedCounter",    1, "str", FuncJoinSharedCounter,    "shared.c:JoinSharedCounter" },
	{ "DestroySharedCounter", 1, "str", FuncDestroySharedCounter, "shared.c:DestroySharedCounter" },
	{ "NextSharedCounter",    0, ""   , FuncNextSharedCounter,    "shared.c:NextSharedCounter" },
	{ "ClearSharedCounter",   0, ""   , FuncClearSharedCounter,   "shared.c:ClearSharedCounter" },
  { 0 }
};

static Int InitKernel (StructInitInfo * module)
{
    InitHdlrFuncsFromTable(GVarFunc);
    return 0;
}

static Int InitLibrary(StructInitInfo * module)
{
    InitGVarFuncsFromTable(GVarFunc);
    return 0;
}

static StructInitInfo module = {
    MODULE_DYNAMIC,
    "shared",
    0,
    0,
    0,
    0,
    InitKernel,
    InitLibrary,
    0,
    0,
    0,
    0
};

StructInitInfo * Init__Dynamic(void)
{
    return &module;
}
