TransposedBravaisRec := function( b )
	local t;

	if not IsRecord( b ) or not IsBound( b.generators ) then
		return fail;
	fi;

	t := rec( generators := List( b.generators, TransposedMat ), size := b.size );

	if IsBound( b.formspace ) then
		t.formspace := List( b.formspace, TransposedMat );
	fi;

	if IsBound( b.normalizer ) then
		t.normalizer := List( b.normalizer, TransposedMat );
	fi;

	if IsBound( b.centerings ) then
		t.centerings := List( b.centerings, TransposedMat );
	fi;

	if IsBound( b.centralizer ) then
		t.centralizer := List( b.centralizer, TransposedMat );
	fi;

	return t;
end;

RForm := function( grp, mat )
	local r;
	r := Sum( grp, x->TransposedMat(x)*mat*x );
	return r/Gcd(Flat(r));
end;

TraceBifo := function( F1, F2 )
	local S;
	S:= List( [1..Length(F1)], i->List([1..Length(F2)], j->Trace( F1[i]*F2[j] ) ) );
	return S/Gcd(Flat(S));
end;

# grp = H
# bgrp = G
Normalisator := function( grp )
	local grec, brec, gbrec, bgrp, trec, tmp, tmp2, arec, A, Vanz, prime, vor;

	if not IsIntegerMatrixGroup( grp ) then
		return fail;
	fi;

	grec := ( generators := GeneratorsOfGroup( grp ), size := Size( grp ) );

	# Calculation of Bravais group of grp
	brec := BravaisGroupFunC( grec, false ); #C:bravais_group(H,FALSE)
	bgrp := Group( brec.generators );

	#Optional - calculate formspace of brec
	if ( brec.formspace = [] ) then
		brec.formspace := FormspaceFunC( brec.generators, 1 ); #C:formspace(G->gen,G->gen_no,1,&G->form_no)
	fi;

	# Transposed Bravais group record
	trec := TransposedBravaisRec( brec );

	# G-perfect form - not set
	# tmp - positive definite G-invariant form
	tmp  := RForm( bgrp, Identity(  bgrp  ) );
	tmp2 := TraceBifo( brec.formspace, trec.formspace );
	arec := FirstPerfectFunC( tmp, brec, trec.formspace, tmp2 ); #C:first_perfect(tmp,G,Gtr->form,tmp2,&Vanz)
	A    := arec.perfect;
	Vanz := arec.min;

	prime:= 1949;
	gbrec:= BravaisGroupFunC( grec, true );
	vor  := NormalizerFunC( A, gbrec, trec, prime, Vanz ); #C:normalizer(A,GB,Gtr,prime,&Vanz)

	brec.normalizer := gbrec.normalizer;

	grec.normalizer := NormalizerInNFunC( grec, brec, false ); #C:normalizer_in_N(H,G,&H->normal_no,FALSE)
	
	return grec;
end;
