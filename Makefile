CFLAGS = -Wall

all: carat.so shared.so redis.so hexstr.so

carat.so: carat.c
	gac -o $@ -d carat.c -p '-march=native -I/home/rlutowsk/carat/include $(CFLAGS)' -L '-L/home/rlutowsk/carat/lib -lpresentation -lfunctions -lpresentation -lgmp -lm_alloc -lm'

shared.so: shared.c
	gac -o $@ -d shared.c -L '-lrt -lpthread' -p $(CFLAGS)

redis.so: redis.c
	gac -o $@ -d redis.c -L '/usr/lib/x86_64-linux-gnu/libhiredis.a' -p $(CFLAGS)

hexstr.so: hexstr.c
	gac -o $@ -d hexstr.c -p $(CFLAGS)

bott.so: CFLAGS := -O3 -Wall 
bott.so: bott.c
	gac -o $@ -d bott.c -p $(CFLAGS)

clean:
	rm $(wildcard *.la *.so)
