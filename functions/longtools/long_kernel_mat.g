# matrix_TYP *long_kernel_mat(A)
# matrix_TYP *A;
#
# long_kernel_mat(A) calculates Matrix X with AX = 0
# the cols of X are a Z-basis of the solution space.
# The functions uses GNU MP

c_long_kernel_mat := function( mat )
	local x;

	x := NullspaceIntMat( TransposedMat( mat ) );

	return TransposedMat( x );
end;
