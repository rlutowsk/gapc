c_formspace_operation := function( f, n )
	local i, res, tmp, n_tr, fno;

	res  := []; #c_init_mat( fno, fno );
	n_tr := c_tr_pose( n );

	for i in [1..fno] do
		tmp    := c_scal_pr( n_tr, f[i] );
		res[i] := c_form_to_vec_modular( tmp, f );
	od;

	return res;
end;

c_position := function( a, x, n )
	local i;

	for i in [1..n] do
		if a[i] = x then
			return i;
		fi;
	od;
	return fail;
end;

c_red_normal := function( g )
	local i, rep, tmp;

	rep := [];

	for i in [1..Length(g.normal)] do
		rep[i] := c_formspace_operation( g.form, g.normal[i] );
	od;

	for i in [2..Length(g.normal)] do
		if c_position( rep, rep[i], i ) <> fail then
			Remove( g.normal, i );
		elif c_position( rep, rep[i]^-1, i ) <> fail then
			Remove( g.normal, i );
		fi;
	od;

	rep := Compacted( g.normal );

	g.normal := rep;
end;
