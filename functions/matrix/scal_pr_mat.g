c_scal_pr := function( vectors, form, truth )
	if truth then
		c_tgauss( vectors );
	fi;
	return vectors * form * TransposedMat( vectors );
end;
