_makeeqlr := function( lm, rm ) # make equation lm*x=x*rm left ordered by rows
	local d, i, j, k, i1, j1, k1, r, c, eq;

	d := Length( lm );
	if d<>Length( lm[1] ) or d<>Length( rm ) or d<>Length( rm[1] ) then
		return fail;
	fi;

	eq := NullMat( d^2, d^2, lm[1][1] );
	for i in [0..d-1] do
		i1 := i+1;
		for j in [0..d-1] do
			j1 := j+1;
			for k in [0..d-1] do
				k1 := k+1;
				r  := d*i+j1;
				c  := d*j+k1;
				eq[r][c] := eq[r][c] + lm[j1][k1];
				c  := d*k+i1;
				eq[r][c] := eq[r][c] - rm[k1][j1];
			od;
		od;
	od;
	return eq;
end;

c_p_solve_eq := function( lmats, rmats )
	local eq, ns, d;

	if not ( Length(lmats)=Length(rmats) and lmats<>[] ) then
		Error("Both list of matrices should be non-empty and of the same size");
	fi;
	eq := Concatenation( List([1..Length(lmats)], i->_makeeqlr(lmats[i], rmats[i])) );

	d  := Length( lmats[1] );
	ns := NullspaceMat( TransposedMat( eq ) );

	return List( ns, r->TransposedMat( List([0..d-1], i->r{[d*i+1..d*(i+1)]}) ) );
end;

c_p_solve_nc := function( lmats, rmats )
	local min, d, lms, rms, nm;

	if lmats = [] and rmats = [] then
		Error("Both list of matrices cannot be empty");
	fi;

	if lmats = [] then
		min := Length( rmats );
		rms := rmats;
		d   := Length( rmats[1] );
		nm  := NullMat( d, d, rmats[1][1][1] );
		lms := List( [1..min], x->nm );
	elif rmats = [] then
		min := Length( lmats );
		lms := lmats;
		d   := Length( lmats[1] );
		nm  := NullMat( d, d, lmats[1][1][1] );
		rms := List( [1..min], x->nm );
	else
		min := Minimum( Length(lmats), Length(rmats) );
		lms := lmats{[1..min]};
		rms := rmats{[1..min]};
	fi;
	return c_p_solve_eq( lms, rms );
end;

c_p_solve := c_p_solve_eq;
