c_row_per := function(mat, i, j)
	local swap, k;

	for k in [1..Length(mat[1])] do
		swap      := mat[i][k];
		mat[i][k] := mat[j][k];
		mat[j][k] := mat[i][k];
	od;
end;

c_col_per := function(mat, i, j)
	local swap, k;

	for k in [1..Length(mat)] do
		swap      := mat[k][i];
		mat[k][i] := mat[k][j];
		mat[k][j] := swap;
	od;
end;

c_row_add := function(mat, i, j, fac)
	local k;

	for k in [1..Length(mat[1])] do
		mat[j][k] := mat[j][k] + fac*mat[i][k];
	od;
end;

c_col_add := function(mat, i, j, fac)
	local k;

	for k in [1..Length(mat)] do
		mat[k][j] := mat[k][j] + fac*mat[k][i];
	od;
end;

c_row_mul := function( mat, i, fac )
	local k;

	for k in [1..Length(mat[1])] do
		mat[i][k] := fac * mat[i][k];
	od;
end;

c_col_mul := function( mat, i, fac )
	local k;

	for k in [1..Length(mat)] do
		mat[k][i] := fac * mat[k][i];
	od;
end;
