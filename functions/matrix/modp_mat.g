#---------------------------------------------------------------------------
# void modp_mat(M, p)
# matrix_TYP *M;
# int p;
#
# reduces the entries of M modulo p such -|p/2| < x < |p/2|
# for every entry x of M.
# if p = 2 or p = -2, the entries are 0 or 1.
#---------------------------------------------------------------------------

c_modp_mat := function( mat, prime )
	local p, mp, r, c, i, j, v;

	p := AbsInt( prime );

	if p = 2 then
		mat := mat mod 2;
		return;
	fi;

	mp:= p/2;
	r := Length( mat );
	c := Length( mat[1] );

	for i in [1..r] do
		for j in [1..c] do
			v := mat[i][j] mod p;
			if v > mp then
				mat[i][j] := v - p;
			else
				mat[i][j] := v;
			fi;
		od;
	od;
end;
