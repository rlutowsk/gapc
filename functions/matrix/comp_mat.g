c_cmp_mat := function( a, b )
	local i,j,mat,m,n;

	mat := a-b;

	m := Length(mat);
	n := Length(mat[1]);

	for i in [1..m] do
		for j in [1..n] do
			if mat[i][j]>0 then
				return 1;
			elif mat[i][j]<0 then
				return -1;
			fi;
		od;
	od;
	return 0;
end;
