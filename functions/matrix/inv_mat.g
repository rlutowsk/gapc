c_rmat_inv := function( mat )
	return mat^-1;
end;

c_imat_inv := c_rmat_inv;
