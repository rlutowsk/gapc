c_divide_by_gcd := function( mat )
	local gcd;

	gcd := Gcd( Flat( mat ) );
	mat := mat / gcd;

	return gcd;
end;
