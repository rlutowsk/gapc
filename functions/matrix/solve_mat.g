c_solve_mat := function( mat )
	return NullspaceIntMat( TransposedMat( mat ) );
end;
