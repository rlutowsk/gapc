c_iset_entry := function( mat, r, c, v )
	local d;

	d := DimensionsMat( mat );
	if r > d[1] or c > d[2] then
		Error("Trying to put an element outside matrix");
	fi;
	mat[r][c] := v;
	return true;
end;

c_rset_entry := c_idet_entry;

c_iscal_mul := function( mat, v )
	return mat * v;
end;

c_rscal_mult := c_iscal_mul;

c_kill_row := function( mat, row )
	if row > Length( mat ) or row <= 0 then
		return false;
	fi;
	Remove(mat, row);
	return true;
end;

c_kill_col := function( mat, col )
	local i;
	if col > Length( mat[1] ) or col <= 0 then
		return false;
	fi;
	for i in [1..Length(mat)] do
		Remove( mat[i], col );
	od;
	return true;
end;

c_ins_row := function( mat, row )
	local i;

	if row > Length( mat ) or row <= 0 then
		return false;
	fi;

	i:=Length( mat )+1;
	while i>row do
		mat[i] := mat[i-1];
		i:=i-1;
	od;
	mat[row] := 0*mat[row];
	return true;
end;

c_ins_col := function( mat, col )
	local i, j, nc;

	nc := Length( mat[1] );
	if col > nc or col <= 0 then
		return false;
	fi;

	nc := nc+1;
	for i in [1..Length(mat)] do
		j := nc;
		while j > col do
			mat[i][j] := mat[i][j-1];
			i:=j-1;
		od;
		mat[i][col] := 0;
	od;
	return true;
end;

c_imul_row := function( mat, row, v )
	mat[row] := v*mat[row];
	return true;
end;

c_rmul_row := c_imul_row;

c_imul_col := function( mat, col, v )
	local n;

	n := Length( mat );

	mat{[1..n]}[col] := mat{[1..n]}[col] * v;

	return true;
end;

c_rmul_col := c_imul_col;

c_iadd_row := function( mat, t, d, v )
	mat[d] := mat[d] + v*mat[t];
	return true;
end;

c_radd_row := c_iadd_row;

c_iadd_col := function( mat, t, d, v )
	local n;
	n := Length( mat );
	mat{[1..n]}[d] := mat{[1..n]}[d] + v * mat{[1..n]}[t];
	return true;
end;

c_radd_col := c_iadd_col;

c_normal_rows := function( mat )
	local r;
	for r in [1..Length(mat)] do
		mat[r] := mat[r] / Gcd(r);
	od;
end;

c_normal_cols := function( mat )
	local i, c, n;

	n := Length( mat );
	for i in [1..Length(mat[1])] do
		c := mat{[1..n]}[i];
		mat{[1..n]}[i] := c / Gcd(c);
	od;
end;

c_mat_to_line := function( gen )
	return List( gen, Flat );
end;

c_line_to_mat := function( mat, row, col )
	return List( mat, m->List([0..row-1], i->m{[col*i+1..col*i+col]}) );
end;

c_normal_mat := function( mat )
	local ggt, r;

	ggt := Gcd( Flat( mat ) );
	for r in [1..Length(mat)] do
		mat[r] := mat[r]/ggt;
	od;
	return ggt;
end;
