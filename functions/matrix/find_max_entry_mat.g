c_find_max_entry := function( mat )
	local i, j, abs, max, m, n;

	max := 0;
	m   := Length( mat );
	n   := Length( mat[1] );

	for i in [1..m] do
		for j in [1..n] do
			if IsInt( mat[i][j] ) then
				abs := AbsInt( mat[i][j] );
				if abs > max then
					max := abs;
				fi;
			else
				return 0;
			fi;
		od;
	od;
	return max;
end;
