c_real_mat := function( mat, rows, cols )
	local nr, nc, nm;

	nr := Minimum( Length(mat), rows );
	nc := Minimum( Length(mat[1]), cols );

	nm := NullMat( rows, cols, mat[1][1] );

	nm{[1..nr]}{[1..nc]} := mat{[1..nr]}{[1..nr]};

	return nm;
end;
