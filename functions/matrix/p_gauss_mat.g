# decide how to treat mod p matrices 
# the best choice seems to be Z(p)
c_p_gauss := function( mat )
	local rk, zr, m; #, i, j, k, l, inv, one, elm;

	TriangulizeMat( mat );

	rk := 1;
	zr := 0 * mat[1];
	m  := Length( mat );

	while rk <= m and mat[rk] <> zr do
		rk := rk+1;
	od;

# TODO: Check if it is really unnecessary
# TODO: TriangulizeMat makes matrix in rre form?
#	rk := rk-1;
#	i  := rk;
#	zr := 0*mat[1][1];
#	m  := Length( mat[1] );
#	one:= One( mat[1][1] );

#	while i>1 do
#		j := 1;
#		while mat[i][j] = zr do; j:=j+1; od;
#		if mat[i][j]<>one then
#			inv := mat[i][j]^-1;
#			mat[i][j] := one;
#			for k in [j+1..m] do; mat[i][k] := inv * mat[i][k]; od;
#		fi;
#		for k in [1..i-1] do
#			if mat[k][j]<>zr then
#				elm := mat[k][j];
#				for l in [j..m] do
#					mat[k][l] := mat[k][l] - elm*mat[i][l];
#				od;
#			fi;
#		od;
#		i := i-1;
#	od;

	return rk-1;
end;

