c_rmat_mul := function( lmat, rmat )
	return lmat * rmat;
end;

c_imat_mul := c_rmat_mul;

c_pmat_mul := function( lmat, rmat, p )
	return ( lmat * rmat ) mod p;
end;

c_mat_mul := c_rmat_mul;

c_mat_muleq := function( lmat, rmat )
	lmat := lmat * rmat;
	return lmat;
end;

c_mat_kon := function( lmat, mmat, rmat )
	return lmat * mmat * rmat;
end;
