c_tgauss := function( mat )
	local rk, zr, m;

	TriangulizeIntegerMat( mat );

	rk := 1;
	zr := 0 * mat[1];
	m  := Length( mat );

	while rk <= m and mat[rk] <> zr do
		rk := rk+1;
	od;
	return rk-1;
end;

# as far as I can understand - the zero rows are deleted
c_row_gauss := function( mat )
	local rk, i;

	rk := c_tgauss( mat ); # just for now
	i  := Length( mat );

	while ( i > rk ); do
		Remove( mat, i );
		i := i-1;
	od;
	return rk;
end;

c_ggauss := function( mat )
	local elt;

	elt := c_copy_mat( mat );

	c_row_gauss( elt );

	return elt;
end;
