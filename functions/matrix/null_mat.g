c_save_null_mat := function( mat )
	local r, c, i, j;

	r := Length( mat );
	c := Length( mat[1] );

	for i in [1..r] do
		for j in [1..c] do
			if mat[i][j]<>0 then; return false; fi;
		od;
	od;

	return true;
end;

c_null_mat := c_save_null_mat;

c_quick_null_mat := c_save_null_mat;
