p_mat_det := function( mat, prime )
	return DeterminantIntMat(mat) mod prime;
end;
