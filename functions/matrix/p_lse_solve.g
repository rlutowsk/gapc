#  The function 'matrix_TYP **p_lse_solve(A, B, anz, p)'
#  calculates for given matrices A and B the solutions of
#              A * X = B  (modulo p)
#  where A is a (n x m)- matrix and B a (n x r) matrix with integral
#  entries (treated as elements of the field with p elements).
#  The number of the returned matrices is given back by the pointer 'anz'.
#
#  If no solution exists a NULL-pointer is returned, and 'anz' is
#  set to 0.
#
#  If a solution of the inhomogenous equation exists, it is
#  returned via the (m x r)-matrix X[0] ( X is the matrix_TYP ** returned by
#  the functions.
#  The (1 x m)-matrices X[1],...,X[anz-1] form a basis of the solutions of
#  the transposed homogenous equation X * A^{tr} = 0.

c_p_lse_solve := function(A, b, p)
	local mat, vec, x, i, ns;

	mat := TransposedMat( A ) * (Z(p)^0);
	if b <> [] then
		vec := TransposedMat( b ) * (Z(p)^0);
	else
		vec := NullMat(1,Length(mat[1])) * Z(p);
	fi;

	x   := [];
	x[1]:= [];

	for i in [1..Length(vec)] do
		x[1][i] := SolutionMat( mat, vec[i] );
		if x[1][i] = fail then
			return []; # should be fail
		fi;
	od;

	ns := NullspaceMat( mat );

	x[1] := List( TransposedMat(x[1]), row->List(row, Int) );

	for i in [1..Length(ns)] do
		x[i+1] := [ List(ns[i], Int) ];
	od;

	return x;
end;
