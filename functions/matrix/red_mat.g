# not quite know what is it for
# assume both matrices symmetric???
c_sdec_mat := function( mat, trf )
	local i, dim, j, flag, m, m1, t1;

	m   := mat;
	dim := Length( mat[1] );

	flag:= (m[dim][dim] = 0);

	while flag do
		i := 1;
		while flag and i<dim do
			flag := ( m[dim][i] = 0 );
			i := i+1;
		od;
		if flag then
			dim  := dim - 1;
			flag := ( dim > 1 ) and ( m[dim][dim] = 0 );
		fi;
	od;
	# TODO: check is it just resizing
	for i in [1..dim] do
		mat[i] := mat[i]{[1..dim]};
		trf[i] := trf[i]{[1..dim]};
	od;
	for i in [dim+1..Length( mat )] do
		Remove( mat );
		Remove( trf );
	od;

	m1 := [];
	t1 := [];

	for i in [1..dim] do
		m1[i] := mat[dim+1-i];
		t1[i] := trf[dim+1-i];
	od;
	mat := m1;
	trf := t1;

	dim := dim+1;
	m1[dim] := [];
	for j in [1..dim-1] do
		for i in [1..dim-1] do
			m1[dim][i] := mat[j][dim-i];
		od;
		for i in [1..dim-1] do
			mat[j][i] := m1[dim][i];
		od;
	od;
end;

c_smat_red := function( m )
	local m1, per, temp, i, j, k, l, flag, faktor, n, t, t1, tam, zr;

	n := Length( m[1] );
	t := IdentityMat( n );

	repeat
		flag := false;
		i := n;
		while i >= 1 do
			j := n;
			while j >= 1 do
				if i<>j then
					if m[i][i]<>0 then
						if AbsInt( 2*m[i][j] ) > AbsInt( m[i][i] ) then
							faktor := Int( 2*m[i][j]/m[i][i] );
							faktor := Int( faktor / 2 ) + SignInt( faktor )*( faktor mod 2 );
							for k in [1..n] do
								m[j][k] := m[j][k] - faktor * m[i][k];
								t[j][k] := t[j][k] - faktor * t[i][k];
							od;
							for k in [1..n] do
								m[k][j] := m[k][j] - faktor * m[k][i];
							od;
							flag := true;
						fi;
					else
						if m[i][j]<>0 then
							faktor := Int( m[j][j]/(m[i][j]*2) );
							if faktor <> 0 then
								for k in [1..n] do
									m[j][k] := m[j][k] - faktor * m[i][k];
									t[j][k] := t[j][k] - faktor * t[i][k];
								od;
								for k in [1..n] do
									m[k][j] := m[k][j] - faktor * m[k][i];
								od;
								flag := true;
							fi;
							if m[j][j] = 0 then
								for k in [1..n] do
									if i<>k and m[k][j]<>0 then
										faktor := Int( m[k][j] / m[i][j] );
										if faktor <> 0 then
											for l in [1..n] do
												m[k][l] := m[k][l] - faktor * m[i][l]; # BUG in carat c file
												t[k][l] := t[k][l] - faktor * t[i][l];
											od;
											for l in [1..n] do
												m[l][k] := m[l][k] - faktor * m[l][i];
											od;
											flag := true;
										fi;
									fi;
								od;
							fi;
						fi;
					fi;
				fi;
				j := j-1;
			od;	
			i := i-1;
		od;
	until flag;
#
# Sorting mat in decreasing order of abs of diag elms
#
	per := [1..n];
	zr  := 0*m[1];
	SortParallel( DiagonalOfMat( m ), per, function(x,y) return AbsInt(x)>AbsInt(y); end );
	# now put zero rows at the end
	i  := n;
	while m[per[i]][per[i]] = 0 do; i:=i-1; od;
	i  := i+1;
	while i<=n and m[per[i]]<>zr do; i := i+1; od;
	if i<n then
		j := i+1;
		while j<=n do
			if m[per[j]]<>zr then
				temp := per[i]; per[i] := per[j]; per[j] := temp;
				i := i+1;
				j := i;
			fi;
			j := j+1;
		od;
	fi;

	m1 := ShallowCopy( m );
	t1 := ShallowCopy( t );
	for i in [1..n] do
		m[i] := m1[per[i]];
		t[i] := t1[per[i]];
		temp := ShallowCopy( m[i] );
		for j in [1..n] do
			m[i][j] := temp[per[j]];
		od;
	od;

	return t;
end;
