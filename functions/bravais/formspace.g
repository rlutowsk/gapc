#---------------------------------------------------------------------------
# matrix_TYP **formspace(B, Banz, sym_opt, fdim)
# matrix_TYP **B;
# int Banz, sym_opt, *fdim;
#
#  formspace calculates a basis of the lattice of integral matrices X with
#      B[i]^{tr} * X * B[i] = X    for 0 <= i < Banz
#  The number of basis elements is returned via (int *fdim).
#  'sym_opt' must be 0, 1 or -1.
#  sym_opt = 0:  the full lattice is calculated.
#  sym_opt = 1:  a basis for the symmetric matrices is calculated
#  sym_opt =-1:  a basis for the skewsymmetric matrices is calculated
#
#---------------------------------------------------------------------------

c_formspace := function( b, sym_opt )
	local banz, i,j,k,l,m,no, dim, dd, anz, x, xk, e, xx, bi, pos, sign;

	dim := DimensionsMat( b[1] );
	if dim[1]<>dim[2] then
		Error("error in formspace: non-square matrix in group b");
	fi;
	dim := dim[1];
	banz:= Length( b );

	for i in [2..banz] do
		j := DimensionsMat( b[i] );
		if j[1]<>dim or j[2]<>dim then
			Error("error in formspace: different dimesion of group elements");
		fi;
	od;

	if sym_opt = 1 then
		dd := dim * (dim+1) / 2;
	elif sym_opt = -1 then
		dd := dim * (dim-1) / 2;
	else
		sym_opt := 0;
		dd := dim * dim;
	fi;

	pos := NullMat(dim,dim);
	sign:= NullMat(dim,dim);

	if sym_opt = 0 then
		no := 0;
		for i in [1..dim] do
			for j in [1..dim] do
				pos[i][j] := no; sign[i][j] := 1; no := no+1;
			od;
		od;
	elif sym_opt = 1 then
		no := 0;
		for i in [1..dim] do
			for j in [1..i] do
				pos[i][j] := no; sign[i][j] := 1; no := no+1;
			od;
		od;
		no := 0;
		for i in [1..dim] do
			for j in [1..i] do
				pos[j][i] := no; sign[j][i] := 1; no := no+1;
			od;
		od;
	else # sym_opt = -1
		no := 0;
		for i in [1..dim] do
			for j in [1..i-1] do
				pos[i][j] := no; sign[i][j] := 1; no := no+1;
			od;
		od;
		no := 0;
		for i in [1..dim] do
			for j in [1..i-1] do
				pos[j][i] := no; sign[j][i] := -1; no := no+1;
			od;
		od;
		# really not necessary - we have NullMats
		#for i in [1..dim] do
		#	pos[i][i] := 0; sign[i][i] := 0;
		#od;
	fi;

	x  := NullMat( dd * banz, dd );
	xx := x;
	no := 1;

	if sym_opt = 0 then
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..dim] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k];
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no := no+1;
				od;
			od;
		od;
	elif sym_opt = 1 then
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..j] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k];
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no := no+1;
				od;
			od;
		od;
	else # sym_opt = -1
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..j-1] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k];
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no := no+1;
				od;
			od;
		od;
	fi;

	xk := c_long_kernel_mat( x );
	if xk = [] then
		anz := 0;
		e   := fail;
	else
		anz := Length( xk[1] );
		e   := [];
	fi;
	for i in [1..anz] do
		e[i] := NullMat( dim, dim );
		if sym_opt = 0 then
			for j in [1..dim] do
				for k in [1..dim] do
					e[i][j][k] := xk[pos[j][k]][i];
				od;
			od;
		elif sym_opt = 1 then
			for j in [1..dim] do
				for k in [1..j] do
					e[i][j][k] := xk[pos[j][k]][i];
					if j<>k then
						e[i][k][j] := e[i][j][k];
					fi;
				od;
			od;
		else # sym_opt = -1
			for j in [1..dim] do
				for k in [1..j-1] do
					e[i][j][k] := xk[pos[j][k]][i];
					e[i][k][j] := -e[i][j][k];
				od;
			od;
			# making zero of diag of e[i] done
		fi;
	od;
	for i in [1..anz] do
		j := 1;
		while j<=dim and e[i][j][j]=0 do; j:=j+1; od;
		if j<=dim and e[i][j][j]<0 then
			e[i] := -e[i];
		fi;
	od;
	return e;
end;
