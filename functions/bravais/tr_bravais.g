# bravais_TYP *tr_bravais(B, calcforms, invert)
# bravais_TYP *B;
# int calcforms;
# int invert;
#
#  'tr_bravais' caclulates the group G =  B^{tr}
#  The matrices in B->gen, B->zentr, B->normal and B->cen are transposed
#  (if exists)
#  If calcforms == 0, then the matrices of G->form are not calculated
#  If calcforms == 1, then G->form is calculated with 'formspace'
#  If calcforms == 2, then G->form is calculated with 'invar_space'
#                     if B->form_no != 0, invar_space is started with
#                     fdim = B->form_no, otherwise the argument 'fdim'
#                     for invar_space is calculated by calculating the
#                     formspace with p_formspace modulo 101.
#  If (invert == TRUE) the matrices in G->gen,G->cen,G->normal
#  are inverted.

tr_bravais := function( b, calcforms, invert )
	local g, i,j,k, xx,fd;

	g := c_init_bravais( b.dim );

	g.group := TransposedMatrixGroup( b.group );

	if invert then
		g.gen    := List( b.gens,  x->TransposedMat( x^-1 ) );
		g.normal := List( b.normal,x->TransposedMat( x^-1 ) );
		g.cen    := List( g.cen,   x->TransposedMat( x^-1 ) );
	else
		g.gen    := List( b.gens,  TransposedMat );
		g.normal := List( b.normal,TransposedMat );
		g.cen    := List( g.cen,   TransposedMat );
	fi;
	g.zentr := List( b.zentr, TransposedMat );

	if calcforms = 1 then
		g.form := c_formspace( g.gen, 1 );
	fi;
	if calcforms = 2 then
		fd := Length( b.form );
		if fd <> 0 then
			g.form := c_invar_space( g.gen, fd, 1 );
		else
			xx := c_p_formspace( g.gen, 101, 1 );
			g.form := c_invar_space( g.gen, Length(x), 1 );
		fi;
	fi;
	return g;
end;
