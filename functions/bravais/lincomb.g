c_vec_to_form := function( v, f )
	local l;

	l := Length(v);
	if l <> Length(f) then
		Error("both lists should have the same length");
	fi;
	return Sum([1..l], i->v[i]*f[i] );
end;

c_form_to_vec := function( a, f )
	local mat, vec;

	mat := List( f, Concatenation );
	vec := Concatenation( a );

	return SolutionMat( mat, vec );
end;

# SKIPPED
# c_form_to_vertex 

c_form_to_vec_modular := function( a, f )
	local mat, vec;

	mat := List( f, Concatenation );
	vec := Concatenation( a );

	return SolutionIntMat( mat, vec );
end;
