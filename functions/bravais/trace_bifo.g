c_trace_bifo := function( f1, f2 )
	local n, s;
	n := Length(f1);
	if n<>Length(f2) then
		Error("both arguments must have the same length");
	fi;
	s := List([1..n], i->List([1..n], j->Trace(f1[i]*f2[j]) ) );

	return s / Gcd( Flat( s ) );
end;
