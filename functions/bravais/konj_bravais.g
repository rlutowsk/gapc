c_konj_bravais := function( b, t )
	local ti, titr, g;

	ti       := t^-1;
	g        := c_init_bravais(b.dim);

	g.group  := TransposedMatrixGroup( b.group );
	g.gen    := List( b.gen, m->t * m * ti );

	titr     := c_tr_pose( ti );
	g.form   := List( b.form  , m->titr * m * ti );
	g.zentr  := List( b.zentr , m->t * m * ti );
	g.normal := List( b.normal, m->t * m * ti );
	g.cen    := List( b.cen   , m->t * m * ti );

	return g;
end;
