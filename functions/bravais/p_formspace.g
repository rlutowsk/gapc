c_p_formspace := function( b, prime, sym_opt )
	local i,j,k,l,m,no,p,dim,dd,anz,x,xk,e,xx,bi,pos,sign,banz;

	p   := prime;
	dim := Length( b[1][1] );
	banz:=Length(b);

	if ( Length(b[1])<>dim ) then
		Error("error in formspace: non-square matrix in group 'b'");
	fi;
	for i in [2..banz] do
		if Length(b[i])<>dim or Length(b[i][1])<>dim then
			Error("error in formspace: different dimension of group elements");
		fi;
	od;

	if sym_opt = 1 then
		dd = dim * (dim+1) / 2;
	fi;
	if sym_opt = -1 then
		dd = dim * (dim-1) / 2;
	fi;
	if sym_opt = 0 then
		dd = dim * dim;
	fi;

	pos  := c_init_mat(dim, dim)+1;
	sign := c_init_mat(dim, dim)+1;

	if sym_opt = 0 then
		no := 1;
		for i in [1..dim] do
			for j in [1..dim] do
				pos[i][j] := no; sign[i][j] := 1; no := no+1;
			od;
		od;
	fi;

	if sym_opt = 1 then
		no := 1;
		# check if this is the same as in original file
		for i in [1..dim] do
			for j in [1..i] do
				pos[i][j] := no; sign[i][j] := 1;
				pos[j][i] := no; sign[j][i] := 1;
				no := no+1;
			od;
		od;
	fi;

	if sym_opt = -1 then
		no := 1;
		for i in [1..dim] do
			for j in [1..i-1] do
				pos[i][j] := no; sign[i][j] := 1;
				pos[j][i] := no; sign[j][i] := -1;
				no := no+1;
			od;
		od;
	fi;

	x  := c_init_mat( dd*banz, dd );
	xx := x;
	no := 1;

	if sym_opt = 0 then
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..dim] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := (xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k]) mod p;
							# xx[no][pos[l][m]] := xx[no][pos[l][m]] mod p;
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no:=no+1;
				od;
			od;
		od;
	fi;

	if sym_opt = 1 then
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..j] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := (xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k]) mod p;
							# xx[no][pos[l][m]] := xx[no][pos[l][m]] mod p;
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no := no+1;
				od;
			od;
		od;
	fi;

	if sym_opt = -1 then
		for i in [1..banz] do
			bi := b[i];
			for j in [1..dim] do
				for k in [1..j-1] do
					for l in [1..dim] do
						for m in [1..dim] do
							xx[no][pos[l][m]] := (xx[no][pos[l][m]] + sign[l][m]*bi[l][j]*bi[m][k]) mod p;
						od;
					od;
					xx[no][pos[j][k]] := xx[no][pos[j][k]]-1;
					no := no+1;
				od;
			od;
		od;
	fi;

	x := x mod p;

	xk := c_p_lse_solve(x, [], p);
	anz:= Length( xk ) - 1;

	e := [];

	for i in [1..anz] do
		e[i] := c_init_mat(dim,dim);
		if sym_opt = 0 then
			for j in [1..dim] do
				for k in [1..dim] do
					e[i][j][k] := xk[i+1][1][pos[j][k]];
				od;
			od;
		fi;
		if sym_opt = 1 then
			for j in [1..dim] do
				for k in [1..j] do
					e[i][j][k] := xk[i+1][1][pos[j][k]];
					if j<>k then
						e[i][k][j] := e[i][j][k];
					fi;
				od;
			od;
		fi;
		if sym_opt = -1 then
			for j in [1..dim] do
				for k in [1..j-1] do
					e[i][j][k] := xk[i+1][1][pos[j][k]];
					e[i][k][j] := -e[i][j][k];
				od;
			od;
			for j in [1..dim] do
				e[i][j][j] := 0;
			od;
		fi;
	od;
	return e;
end;
