# SKIPPED
# ggt
# rmatfac
# rmatone
# rmatadd
# rmatmul
# rmattrans
# gcd
# rond
# chain
# dtoi
# iterate
# symel
# try

c_rform := function( grp, fo )
	local mat;
	mat := Sum( grp, g-> TransposedMat(g) * fo * g );
	return mat / Gcd( Flat( mat ) );
end;
