c_normlin := function( fo, n )
	local i, erg, fdim;

	fdim := Length(fo);

	erg := [];

	for i in [1..fdim] do
		erg[i] := c_form_to_vec_modular( c_tr_pose(n) * fo[i] * n, fo );
		if erg[i] = fail then
			Error("Error in 'normlin': no Z-basis of formspace");
		fi;
	od;
	return erg;
end;
