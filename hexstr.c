#include "src/compiled.h"
#include <stdio.h>
#include <stdlib.h>


Obj HexStringInt2( Obj self, Obj num, Obj len )
{
	UInt n = INT_INTOBJ( num );
	int  l = INT_INTOBJ( len );
	Obj  s = NEW_STRING( l );

	snprintf(CSTR_STRING(s), l+1, "%0*lX", l, n);

	return s;	
}

static StructGVarFunc GVarFunc[] = {
	{ "HexStringInt2" , 2, "num, len", HexStringInt2, "hexstr.c:HexStringInt2" },
	{ 0 }
};

static Int InitKernel (StructInitInfo * module)
{
    InitHdlrFuncsFromTable(GVarFunc);
    return 0;
}

static Int InitLibrary(StructInitInfo * module)
{
    InitGVarFuncsFromTable(GVarFunc);
    return 0;
}

static StructInitInfo module = {
    MODULE_DYNAMIC,
    "shared",
    0,
    0,
    0,
    0,
    InitKernel,
    InitLibrary,
    0,
    0,
    0,
    0
};

StructInitInfo * Init__Dynamic(void)
{
    return &module;
}

